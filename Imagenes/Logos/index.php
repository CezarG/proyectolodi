<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>ERP - GrupoLodi</title>

<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../assets/css/form-elements.css">
<link rel="stylesheet" href="../assets/css/style.css">
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>

<body>

  <!-- Top content -->
  <div class="top-content">

      <div class="inner-bg">
          <div class="container">

                  <div align="center" class="col-lg-12">
                      <img src="../assets/images/logo.png" class="img-responsive" width="410px" />
                  </div>

              <div class="row col-lg-12">
                  
              </div>

          </div>
      </div>

  </div>

  <!-- Javascript -->
  <script src="../assets/js/jquery-1.11.1.min.js"></script>
  <script src="../assets/js/bootstrap.min.js"></script>
  <script src="../assets/js/jquery.backstretch.min.js"></script>
  <script src="../assets/js/scripts0.js"></script>

  <!--[if lt IE 10]>
      <script src="assets/js/placeholder.js"></script>
  <![endif]-->



</body>
</html>

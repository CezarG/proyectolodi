<page orientation="paysage" style="font-size: 7px" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
  <page_header>
    <table style="width: 100%; border: solid 0px #000;">
      <tr >
        <td  style="width: 49.5%; border: solid 1px #000;">
          <table align="center" style=" margin-top: 1px; border: solid 1.1px #000;">
          <tr>
            <td align="center" height="60" width="200" rowspan="3"><img src="<?php echo $rutaimg; ?>" height="45" width="180"/></td>
            <td align="center" width="310"><span class="Estilo0"><u>BOLETA DE PAGO DE REMUNERACIONES</u></span></td>
          </tr>
          <tr align="center">
            <td><span class="Estilo03">Decreto Supremo Nro 01-98 TR del 22.01.98</span> </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table align="center" height="92" style=" border: solid 1.1px #000;">
        <tr>
          <td width="350"><span class="Estilo0">Raz&oacute;n Social:<strong> <?php echo $nomepm; ?></strong></span></td>
          <td width="160"><span class="Estilo0">R.U.C. <strong><?php echo $rucepm; ?></strong></span></td>
        </tr>
        <tr>
          <td colspan="2"><span class="Estilo0">Direcci&oacute;n: JR. EDGAR ZUÑIGA 165 - SAN LUIS</span></td>
        </tr>
        <tr>
          <td  colspan="2"><span class="Estilo0">Mes y A&ntilde;o: <i><?php echo $mes.' '.$anual; ?></i></span></td>
          </tr>
      </table>
      <table align="center" style="margin-top: 2px; border: solid 1.1px #000;">
   <tr>
     <td  colspan="3"><span class="Estilo00">DATOS DEL TRABAJADOR</span></td>
   </tr>
   <tr>
     <td width="516"  colspan="3"><span class="Estilo0">Apellidos y Nombres: <strong><?= $xnomape; ?></strong></span></td>
   </tr>
   <tr>
     <td colspan="2"><span class="Estilo0">Total Remun. S/ <strong><?= $xsueldo; ?></strong></span></td>
     <td ><span class="Estilo0">DNI <strong><?= $xdni; ?></strong></span></td>
   </tr>
   <tr>
     <td colspan="3"><span class="Estilo0">Estableci. JR. EDGAR ZUÑIGA 165 - SAN LUIS</span></td>
     </tr>
   <tr>
     <td><span class="Estilo0">Fec Nacim. <?= $xfecnac; ?></span></td>
     <td><span class="Estilo0">Tipo Traba. 21-Empleado </span></td>
     <td><span class="Estilo0">Afiliacion: <strong><?php if ($tipfon !='SIN PAGO') { echo $xfondo; } ?></strong></span></td>
   </tr>
   <tr>
     <td colspan="2"><span class="Estilo0">Fec. Ingreso <?= $xfecing; ?></span></td>
     <td><span class="Estilo0">Autogenerado.</span></td>
   </tr>
   <?php if ($xperiodo=='MENSUAL'): ?>
     <tr>
       <td width="158"><span class="Estilo0">Fec. Cese CONTINUA</span></td>
       <td width="176"><span class="Estilo0">Dias Subsiadiados </span></td>
       <td><span class="Estilo0">CUSPP <strong><?= $xcuspp; ?></strong></span></td>
     </tr>
     <tr>
       <td><span class="Estilo0">Dias Trabaj. <?= $dias;?></span></td>
       <td><span class="Estilo0">Faltas Justificadas </span></td>
       <td><span class="Estilo01">Cargo <strong><?= $xcargo; ?></strong></span></td>
     </tr>
     <tr>
       <td><span class="Estilo0">Area <strong><?= $xarea; ?></strong></span></td>
       <td><span class="Estilo0">Hrs.Min.Trab. <?=$Nhora.' hrs.';?></span></td>
       <td><span class="Estilo0">Dia No Trab y No Sub:</span></td>
     </tr>
   <?php endif; ?>
   <?php if ($xperiodo=='GRATIFICACION'): ?>
     <tr>
       <td width="158"><span class="Estilo0">Fec. Cese CONTINUA</span></td>
       <td width="176"><span class="Estilo0">&nbsp; </span></td>
       <td><span class="Estilo0">CUSPP <strong><?= $xcuspp; ?></strong></span></td>
     </tr>
     <tr>
       <td width="158"><span class="Estilo0">Meses Lab.</span></td>
       <td width="176"><span class="Estilo0">Cargo <strong><?= $xcargo; ?></strong></span></td>
       <td><span class="Estilo0">Area <strong><?= $xarea; ?></strong></span></td>
     </tr>
   <?php endif; ?>
 </table>
 <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
 <table align="center" style="margin-top: 2px; border: solid 1.1px #000;">
     <tr>
       <td width="186"><span class="Estilo0">Tot. Sobretiempo: 0hrs. 0 min. </span></td>
       <td width="146"><span class="Estilo0">H.E. 25%: 0 hrs. 0 min. </span></td>
       <td width="172"><span class="Estilo0">H.E. 35%: 0 hrs. 0 min. </span></td>
     </tr>
     <tr>
       <td>&nbsp;</td>
       <td><span class="Estilo0">H.E. 60%: 0 hrs. 0 min. </span></td>
       <td><span class="Estilo0">H.E. 100%: 0 hrs. 0 min. </span></td>
     </tr>
   </table>
  <?php endif; ?>
   <table align="center" style="margin-top: 2px;">
     <tr align="center">
       <td class="tborde" width="167"><span class="Estilo00">REMUNERACIONES</span></td>
       <td class="tborde" width="155"><span class="Estilo00">DESCUENTOS</span></td>
       <td class="tborde" width="120"><span class="Estilo00">APORTACIONES</span></td>
     </tr>
       <tr>
          <td >
            <table>
              <?php   while($rerow=mysqli_fetch_array($rremunera,MYSQLI_ASSOC)){ ?>
                  <tr>
                    <td class="tbordee" width="135" ><span class="Estilo03"><?php echo $rerow['histmov_nombre'];  ?></span></td>
                    <td align="right" class="tbordee" width="32" ><span class="Estilo03"><?php echo $rerow['histmov_monto'];  ?></span></td>
                  </tr>
                <?php }  ?>
                <!-- Catidad de filas vacias -->
                <?php $r = 1;
                  while ($r <= $nt) { ?>
                <tr>
                    <td class="tbordee"><span class="Estilo03">&nbsp;</span></td>
                </tr>
                 <?php $r++; } ?>
                </table>
          </td>
          <td>
            <table>
              <?php   while($derow=mysqli_fetch_array($rdescu,MYSQLI_ASSOC)){ ?>
                  <tr>
                    <td class="tbordee" width="110"><span class="Estilo03"><?= $derow['histmov_nombre'];  ?></span></td>
                    <td align="right" class="tbordee" width="42"><span class="Estilo03"><?= $derow['histmov_monto'];  ?></span></td>
                  </tr>
                <?php } ?>
                <!-- Catidad de filas vacias -->
                <?php $d = 1;
                  if ($ntt=='') { $ntt=7; $ancho='width="142"'; $ancho2='width="114"';}
                  while ($d <= $ntt) { ?>
                <tr>
                  <td class="tbordee" <?= $ancho ?>><span class="Estilo03">&nbsp;</span></td>
                </tr>
                 <?php $d++; } ?>
                </table>
          </td>
          <td>
            <table >
                 <tr>
                   <td class="tbordee" width="100"><span class="Estilo03"><?php echo $appor;  ?></span></td>
                   <td align="right" class="tbordee" width="29"><span class="Estilo03"><?php echo $montoa;  ?></span></td>
                  </tr>
                <!-- Catidad de filas vacias -->
                <?php $ap = 1;
                  while ($ap <= '7') { ?>
                <tr>
                  <td class="tbordee" <?= $ancho2;?>><span class="Estilo03">&nbsp;</span></td>
                </tr>
                 <?php $ap++; } ?>
                </table>
          </td>
        </tr>
   </table>
   <table align="center">
     <tr>
       <td width="260" class="tborde"><span class="Estilo0">Tot. Afecto a Desc.&nbsp;&nbsp;<?php echo $ttrem; ?>&nbsp;&nbsp;&nbsp;&nbsp;(*) Conceptos Afectos </span></td>
       <td width="250"></td>
     </tr>
   </table>
   <table align="center">
     <tr>
       <td class="tborde" align="right" width="165"><span class="Estilo0">Tot. Remuneraci&oacute;n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $ttrem; ?></span></td>
       <td class="tborde" align="right" width="165"><span class="Estilo0">Total Descuento &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ttdesc; ?></span></td>
       <td class="tborde" align="right" width="165"><span class="Estilo0">Tot. Aportac. &nbsp;&nbsp;&nbsp;&nbsp;<?=$montoa; ?></span></td>
     </tr>
     <tr>
       <td align="right"><span class="Estilo0">Tot. EFECTIVO <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  $total=$ttrem-$ttdesc;   echo number_format($total, 2, '.', ''); ?></strong></span></td>
       <td align="right"><span class="Estilo0">Tot. ALIMENTOS 0.00 </span></td>
       <td align="right"><span class="Estilo0">Tot. CANASTA 0.00 </span></td>
     </tr>
   </table>
   <table align="center" style="margin-top: 0px; border: solid 0px #000;">
     <tr>
       <td  width="170"><span class="Estilo00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo  $mes.', '.' del '.$anual; ?></span></td>
       <td align="right" width="164"><span class="Estilo00"><strong>Neto Recibido </strong></span></td>
       <td align="right" width="170"><span class="Estilo00"><strong><u>S/ &nbsp;&nbsp; <?php echo number_format($total, 2, '.', ''); ?></u></strong></span></td>
     </tr>
   </table>
   <?php if ($xperiodo=='GRATIFICACION'): echo '<br><br>'; endif; ?>
   <table align="center" style="margin-top: 16px; border: solid 0px #000;">
     <tr align="center">
       <td width="180">&nbsp;</td>
       <td width="143"><span class="Estilo0">Recibi Conforme </span></td>
       <td width="180">&nbsp;</td>
     </tr>
     <tr align="center">
       <td width="180">&nbsp;</td>
       <td width="143">&nbsp;</td>
       <td width="180">&nbsp;</td>
     </tr>
     <tr align="center">
       <td width="180">&nbsp;</td>
       <td width="143">&nbsp;</td>
       <td width="180">&nbsp;</td>
     </tr>
     <tr align="center">
       <td>.............................................</td>
       <td>&nbsp;</td>
       <td>.............................................</td>
     </tr>
     <tr align="center">
       <td><span class="Estilo1">Empleador</span></td>
       <td>&nbsp;</td>
       <td><span class="Estilo1">Firma del Trabajador</span> </td>
     </tr>
     <tr align="center">
       <td><span class="Estilo1"><?php
       if ($xcargo<>'GERENTE') {
         $sqlfirma="select histdet_ndoc,histdet_nomape FROM sys_rh_historico_detalle WHERE histdet_cargo='GERENTE'";
         $rfirma  = mysqli_query($con,$sqlfirma);
         $rsfirma=mysqli_fetch_array($rfirma,MYSQLI_ASSOC);
         $firman = $rsfirma['histdet_nomape'];
         $firmann= 'D.N.I:'.$rsfirma['histdet_ndoc'];

       }else {
         $firman="";
         $firmann="";
       } echo $firman;
         ?></span></td>
       <td>&nbsp;</td>
       <td><span class="Estilo1"><?php echo $xnomape; ?></span></td>
     </tr>
     <tr align="center">
       <td><span class="Estilo1"><?php echo $firmann; ?></span></td>
       <td>&nbsp;</td>
       <td><span class="Estilo1">D.N.I:<?php echo $xdni; ?></span></td>
     </tr>
   </table><?php if ($xperiodo=='GRATIFICACION'): echo '<br><br><br>'; endif; ?>&nbsp;  </td>
      <td  align="center" style="width: 1%; border: solid 0px #00FF00;">
        <div align="center" class="vl">&nbsp;&nbsp;</div>
      </td>
      <td style="width: 49.5%; border: solid 1px #000;">
            <table align="center" style=" margin-top: 1px; border: solid 1.1px #000;">
            <tr>
              <td align="center" height="60" width="200" rowspan="3"><img src="<?= $rutaimg; ?>" height="45" width="180"/></td>
              <td align="center" width="310"><span class="Estilo0"><u>BOLETA DE PAGO DE REMUNERACIONES</u></span></td>
            </tr>
            <tr align="center">
              <td><span class="Estilo03">Decreto Supremo Nro 01-98 TR del 22.01.98</span> </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <table align="center" height="92" style=" border: solid 1.1px #000;">
          <tr>
            <td width="350"><span class="Estilo0">Raz&oacute;n Social:<strong> <?=$nomepm; ?></strong></span></td>
            <td width="160"><span class="Estilo0">R.U.C. <strong><?= $rucepm; ?></strong></span></td>
          </tr>
          <tr>
            <td colspan="2"><span class="Estilo0">Direcci&oacute;n: JR. EDGAR ZUÑIGA 165 - SAN LUIS</span></td>
          </tr>
          <tr>
            <td  colspan="2"><span class="Estilo0">Mes y A&ntilde;o: <i><?php echo $mes.' '.$anual; ?></i></span></td>
            </tr>
        </table>
        <table align="center" style="margin-top: 2px; border: solid 1.1px #000;">
     <tr>
       <td  colspan="3"><span class="Estilo00">DATOS DEL TRABAJADOR</span></td>
     </tr>
     <tr>
       <td width="516"  colspan="3"><span class="Estilo0">Apellidos y Nombres: <strong><?= $xnomape; ?></strong></span></td>
     </tr>
     <tr>
       <td colspan="2"><span class="Estilo0">Total Remun. S/ <strong><?= $xsueldo; ?></strong></span></td>
       <td ><span class="Estilo0">DNI <strong><?= $xdni; ?></strong></span></td>
     </tr>
     <tr>
       <td colspan="3"><span class="Estilo0">Estableci. JR. EDGAR ZUÑIGA 165 - SAN LUIS</span></td>
       </tr>
     <tr>
       <td><span class="Estilo0">Fec Nacim. <?= $xfecnac; ?></span></td>
       <td><span class="Estilo0">Tipo Traba. 21-Empleado </span></td>
       <td><span class="Estilo0">Afiliacion: <strong><?php if ($tipfon !='SIN PAGO') { echo $xfondo; } ?></strong></span></td>
     </tr>
     <tr>
       <td colspan="2"><span class="Estilo0">Fec. Ingreso <?php echo $xfecing; ?></span></td>
       <td><span class="Estilo0">Autogenerado.</span></td>
     </tr>

     <?php if ($xperiodo=='MENSUAL'): ?>
       <tr>
         <td width="158"><span class="Estilo0">Fec. Cese CONTINUA</span></td>
         <td width="176"><span class="Estilo0">Dias Subsiadiados </span></td>
         <td><span class="Estilo0">CUSPP <strong><?= $xcuspp; ?></strong></span></td>
       </tr>
       <tr>
         <td><span class="Estilo0">Dias Trabaj. <?= $dias;?></span></td>
         <td><span class="Estilo0">Faltas Justificadas </span></td>
         <td><span class="Estilo01">Cargo <strong><?= $xcargo; ?></strong></span></td>
       </tr>
       <tr>
         <td><span class="Estilo0">Area <strong><?= $xarea; ?></strong></span></td>
         <td><span class="Estilo0">Hrs.Min.Trab. <?=$Nhora.' hrs.';?></span></td>
         <td><span class="Estilo0">Dia No Trab y No Sub:</span></td>
       </tr>
     <?php endif; ?>
     <?php if ($xperiodo=='GRATIFICACION'): ?>
       <tr>
         <td width="158"><span class="Estilo0">Fec. Cese CONTINUA</span></td>
         <td width="176"><span class="Estilo0">&nbsp; </span></td>
         <td><span class="Estilo0">CUSPP <strong><?= $xcuspp; ?></strong></span></td>
       </tr>
       <tr>
         <td width="158"><span class="Estilo0">Meses Lab.</span></td>
         <td width="176"><span class="Estilo0">Cargo <strong><?= $xcargo; ?></strong></span></td>
         <td><span class="Estilo0">Area <strong><?= $xarea; ?></strong></span></td>
       </tr>
     <?php endif; ?>
   </table>
   <?php if ($xperiodo=='MENSUAL' OR $xperiodo=='CTS'): ?>
   <table align="center" style="margin-top: 2px; border: solid 1.1px #000;">
       <tr>
         <td width="186"><span class="Estilo0">Tot. Sobretiempo: 0hrs. 0 min. </span></td>
         <td width="146"><span class="Estilo0">H.E. 25%: 0 hrs. 0 min. </span></td>
         <td width="172"><span class="Estilo0">H.E. 35%: 0 hrs. 0 min. </span></td>
       </tr>
       <tr>
         <td>&nbsp;</td>
         <td><span class="Estilo0">H.E. 60%: 0 hrs. 0 min. </span></td>
         <td><span class="Estilo0">H.E. 100%: 0 hrs. 0 min. </span></td>
       </tr>
     </table>
    <?php endif; ?>
     <table align="center" style="margin-top: 2px;">
       <tr align="center">
         <td class="tborde" width="167"><span class="Estilo00">REMUNERACIONES</span></td>
         <td class="tborde" width="155"><span class="Estilo00">DESCUENTOS</span></td>
         <td class="tborde" width="120"><span class="Estilo00">APORTACIONES</span></td>
       </tr>
         <tr>
            <td >
              <table >
                <?php
                $sqlremu2="SELECT histmov_nombre,truncate(histmov_monto,2) histmov_monto FROM sys_rh_historico_moviento WHERE hist_id ='$id'
                and histmov_ndoc='$xdni' and histmov_tipo =1";
                $rremunera2  = mysqli_query($con,$sqlremu2);
                while($rerow2=mysqli_fetch_array($rremunera2,MYSQLI_ASSOC)){ ?>
                    <tr>
                      <td class="tbordee" width="135" ><span class="Estilo03"><?php echo $rerow2['histmov_nombre'];  ?></span></td>
                      <td align="right" class="tbordee" width="32" ><span class="Estilo03"><?php echo $rerow2['histmov_monto'];  ?></span></td>
                    </tr>
                  <?php }  ?>
                  <!-- Catidad de filas vacias -->
                  <?php $r = 1;
                    while ($r <= $nt) { ?>
                  <tr>
                      <td class="tbordee" ><span class="Estilo03">&nbsp;</span></td>
                  </tr>
                   <?php $r++; } ?>
                  </table>
            </td>
            <td>
              <table>
                <?php
                $sqlrdescu2="SELECT histmov_nombre,truncate(histmov_monto,2) histmov_monto FROM sys_rh_historico_moviento WHERE hist_id ='$id'
                and histmov_ndoc='$xdni' and histmov_tipo =0";
                $rdescu2  = mysqli_query($con,$sqlrdescu2);
                while($derow2=mysqli_fetch_array($rdescu2,MYSQLI_ASSOC)){ ?>
                    <tr>
                      <td class="tbordee" width="110"><span class="Estilo03"><?= $derow2['histmov_nombre'];  ?></span></td>
                      <td align="right" class="tbordee" width="42"><span class="Estilo03"><?= $derow2['histmov_monto'];  ?></span></td>


                    </tr>
                  <?php } ?>
                  <!-- Catidad de filas vacias -->
                  <?php $d = 1;
                    if ($ntt=='') { $ntt=7; $ancho='width="142"'; $ancho2='width="114"';}
                    while ($d <= $ntt) { ?>
                  <tr>
                    <td class="tbordee"  <?=$ancho;?>><span class="Estilo03">&nbsp;</span></td>
                  </tr>
                   <?php $d++; } ?>
                  </table>
            </td>
            <td>
              <table>
                    <tr>
                      <td class="tbordee" width="100"><span class="Estilo03"><?php echo $appor;  ?></span></td>
                      <td align="right" class="tbordee" width="29"><span class="Estilo03"><?php echo $montoa;  ?></span></td>
                    </tr>
                      <!-- Catidad de filas vacias -->
                      <?php $ap = 1;
                        while ($ap <= '7') { ?>
                      <tr>
                        <td class="tbordee" <?=$ancho2;?>><span class="Estilo03">&nbsp;</span></td>
                      </tr>
                       <?php $ap++; } ?>
                  </table>
            </td>
          </tr>
     </table>
     <table align="center">
       <tr>
         <td width="260" class="tborde"><span class="Estilo0">Tot. Afecto a Desc.&nbsp;&nbsp;<?php echo $ttrem; ?>&nbsp;&nbsp;&nbsp;&nbsp;(*) Conceptos Afectos </span></td>
         <td width="250"></td>
       </tr>
     </table>
     <table align="center">
       <tr>
         <td class="tborde" align="right" width="165"><span class="Estilo0">Tot. Remuneraci&oacute;n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $ttrem; ?></span></td>
         <td class="tborde" align="right" width="165"><span class="Estilo0">Total Descuento &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ttdesc; ?></span></td>
         <td class="tborde" align="right" width="165"><span class="Estilo0">Tot. Aportac. &nbsp;&nbsp;&nbsp;&nbsp;<?=$montoa; ?></span></td>
       </tr>
       <tr>
         <td align="right"><span class="Estilo0">Tot. EFECTIVO <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= number_format($total, 2, '.', ''); ?></strong></span></td>
         <td align="right"><span class="Estilo0">Tot. ALIMENTOS 0.00 </span></td>
         <td align="right"><span class="Estilo0">Tot. CANASTA 0.00 </span></td>
       </tr>
     </table>
     <table align="center" style="margin-top: 0px; border: solid 0px #000;">
       <tr>
         <td width="170"><span class="Estilo00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo  $mes.', '.' del '.$anual; ?></span></td>
         <td align="right" width="164"><span class="Estilo00"><strong>Neto Recibido </strong></span></td>
         <td align="right" width="170"><span class="Estilo00"><strong><u>S/&nbsp;&nbsp; <?= number_format($total, 2, '.', ''); ?></u></strong></span></td>
       </tr>
     </table>
      <?php if ($xperiodo=='GRATIFICACION'): echo '<br><br>'; endif; ?>
     <table align="center" style="margin-top: 16px; border: solid 0px #000;">
       <tr align="center">
         <td width="180">&nbsp;</td>
         <td width="143"><span class="Estilo0">Recibi Conforme </span></td>
         <td width="180">&nbsp;</td>
       </tr>
       <tr align="center">
         <td width="180">&nbsp;</td>
         <td width="143">&nbsp;</td>
         <td width="180">&nbsp;</td>
       </tr>
       <tr align="center">
         <td width="180">&nbsp;</td>
         <td width="143">&nbsp;</td>
         <td width="180">&nbsp;</td>
       </tr>
       <tr align="center">
         <td>.............................................</td>
         <td>&nbsp;</td>
         <td>.............................................</td>
       </tr>
       <tr align="center">
         <td><span class="Estilo1">Empleador</span></td>
         <td>&nbsp;</td>
         <td><span class="Estilo1">Firma del Trabajador</span> </td>
       </tr>
       <tr align="center">
         <td><span class="Estilo1"><?php echo $firman; ?></span></td>
         <td>&nbsp;</td>
         <td><span class="Estilo1"><?php echo $xnomape; ?></span></td>
       </tr>
       <tr align="center">
         <td><span class="Estilo1"><?php echo $firmann; ?></span></td>
         <td>&nbsp;</td>
         <td><span class="Estilo1">D.N.I:<?php echo $xdni; ?></span></td>
       </tr>
     </table><?php if ($xperiodo=='GRATIFICACION'): echo '<br><br><br>'; endif; ?>&nbsp;
      </td>
    </tr >
    </table>
  </page_header>
</page>

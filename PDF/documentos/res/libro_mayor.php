<?php
  error_reporting(E_ERROR | E_PARSE);
  include "../../Funciones/BD.php";
  $ruc = $_GET['ruc'];
  $nombre = $_GET['nom'];
  $anual = $_GET['anual'];
  $mes = $_GET['mes']; $xmes = $mes;
  if ($mes >'1') {
    $mes2 = $mes -1;
  } else {
    $mes2 = '12';
    $anual2 = $anual - 1;
  }
  $sqlmes = "SELECT Nmes('$mes') as mes, Nmes('$mes2') as mes_ant";
  $rmes = mysqli_query($con,$sqlmes);
  $ames = mysqli_fetch_array($rmes,MYSQLI_ASSOC);
  $mes = $ames['mes']; $mes_ant = $ames['mes_ant'];


  $emp = $_GET['emp'];
  $opc = $_GET['opcion'];

  $sqlcuenta = "SELECT distinct(asid_cuentad),sp.plade_nombre FROM sys_conta_asientos_general ag,sys_conta_asientos_detalle sd, sys_conta_plan_detalle sp
	WHERE ag.asig_cod = sd.asid_cod and sd.asid_cuentad = sp.plade_codrela  and ag.emp_id = '$emp' AND EXTRACT(YEAR FROM ag.asig_fecha)='$anual'
  AND extract(MONTH FROM ag.asig_fecha)='$xmes'
	ORDER BY asid_cuentad";
  $rgral=mysqli_query($con,$sqlcuenta);

?>
<style type="text/css">
.Estilo0 {font-size: 12px; font-weight: bold; }
.Estilo1 {font-size: 11px; font-weight: bold; }
.Estilo01 {font-size: 10px; font-weight: bold; }
.Estilo11 {font-size: 10px; font-weight: bold;  }
.Estilo12 {font-size: 10px;  }
.Estilo13 {font-size: 9px; font-weight: bold; }
.Estilo14 {font-size: 8px; font-weight: bold; }
.tborde {
  border: 1.1px solid black;
}

</style>
<page orientation="portrait" style="font-size: 7" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
  <?php if ($opc =='basico') { ?>
  <page_header>
<table  border="0">
  <tr>
    <td colspan="2"><span class="Estilo0"><?php echo $nombre; ?></span></td>
    <td width="180" align="right"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
  </tr>
  <tr>
    <td width="180"><span class="Estilo0">R.U.C: <?php echo $ruc; ?></span></td>
    <td width="360" align="center"><span class="Estilo0">LIBRO MAYOR </span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><span class="Estilo0">FORMATO 6.1 </span></td>
    <td align="center"><span class="Estilo0"><?php echo $mes.' '.$anual; ?></span></td>
    <td>&nbsp;</td>
  </tr>
</table>
<hr /><?php  while($row=mysqli_fetch_array($rgral,MYSQLI_ASSOC)){  ?>
<table width="735" border="0">
<tr>
  <td width="270"><span class="Estilo12">CODIGO CUENTA CONTABLE: </span><span class="Estilo11"><?=$row['asid_cuentad']; ?></span></td>
  <td width="465">&nbsp;</td>
</tr>
<tr>
  <td><span class="Estilo12">DENOMINACI&Oacute;N DE LA CUENTA CONTABLE:</span><span class="Estilo11"> <?= $row['plade_nombre']; ?></span></td>
  <td>&nbsp;</td>
</tr>
</table>
<table width="735" height="30" border="1">
<tr align="center">
  <td width="54" rowspan="2"><span class="Estilo13">Fecha Operaci&oacute;n </span></td>
  <td width="70" rowspan="2"><span class="Estilo13">N&ordm; Correlativo Operaci&oacute;n </span></td>
  <td width="360" colspan="2"><span class="Estilo13">Descrici&oacute;n o Glosa de la Operaci&oacute;n </span></td>
  <td width="230" colspan="2"><span class="Estilo13">SALDOS Y MOVIMIENTOS </span></td>
</tr>
<tr align="center">
  <td width="80" height="15"><span class="Estilo13">Documento</span></td>
  <td width="120"><span class="Estilo13">Glosa</span></td>
  <td width="92"><span class="Estilo13">DEUDOR</span></td>
  <td width="92"><span class="Estilo13">ACREEDOR</span></td>
</tr>
</table>
<table width="735" border="0">
<tr align="right">
  <td width="54">&nbsp;</td>
  <td width="70">&nbsp;</td>
  <td width="140">&nbsp;</td>
  <td width="210"><span class="Estilo13">Saldo al Mes APERTURA: </span></td>
  <td width="110"><span class="Estilo13">0.00</span></td>
  <td width="110"><span class="Estilo13">0.00</span></td>
</tr>
<!-- MOVIMIENTO -->
<?php
    $id = $row['asid_cuentad'];
    $sqldeta="SELECT date_format(ag.asig_fecha,'%d/%m%Y') AS fecha,ag.asig_ndoc,sd.asid_haber, sd.asid_debe FROM sys_conta_asientos_detalle sd, sys_conta_asientos_general ag
       where sd.asid_cuentad = '$id' AND sd.asid_cod = ag.asig_cod";
    $rdeta=mysqli_query($con,$sqldeta);
    while($row2=mysqli_fetch_array($rdeta,MYSQLI_ASSOC)){  ?>
<tr align="right">
  <td><span class="Estilo13"><?php echo $row2['fecha']; ?></span></td>
  <td>&nbsp;</td>
  <td><span class="Estilo13"><?php echo $row2['asig_ndoc']; ?></span></td>
  <td>&nbsp;</td>
  <td><span class="Estilo13"><?php $debe =$row2['asid_debe'];
    if ($debe>'0') {
      echo $debe;
    }
  ?></span></td>
  <td><span class="Estilo13"><?php $haber =$row2['asid_haber'];
    if ($haber>'0') {
      echo $haber;
    }
  ?></span></td>
</tr>
<?php } ?>
<!--- --->
<?php
$id = $row['asid_cuentad'];
$sqltdebe = "SELECT SUM(asid_debe) as tdebe,SUM(asid_haber) as thaber FROM sys_conta_asientos_detalle WHERE asid_cuentad = '$id'";
$rtdebe=mysqli_query($con,$sqltdebe);
$rdebe=mysqli_fetch_array($rtdebe,MYSQLI_ASSOC);
$tdebe=$rdebe['tdebe'];
$thaber=$rdebe['thaber'];
?>
<tr align="right">
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><span class="Estilo13">Total Movimiento: </span></td>
  <td><span class="Estilo13"><?php echo $tdebe; ?></span></td>
  <td><span class="Estilo13"><?php echo $thaber; ?></span></td>
</tr>
<tr align="right">
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><span class="Estilo13">Saldo del Periodo del Reporte: </span></td>
  <td><span class="Estilo13"><?php echo $tdebe; ?></span></td>
  <td><span class="Estilo13"><?php echo $thaber; ?></span></td>
</tr>
<tr align="right">
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><span class="Estilo13"><?php echo 'TOTALES (Acumulado a '.$mes.'):'; ?></span></td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr align="right">
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td><span class="Estilo13"><?php echo 'SALDO A '.$mes.':'; ?></span></td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr align="right">
  <td  colspan="2">&nbsp;</td>
  <td  colspan="4">___________________________________________________________________________________</td>
</tr>
</table>

<?php } ?>
</page_header>
  <?php } if ($opc=="sunat"){ ?>
<page_header>
      <table width="834" border="0">
        <tr>
          <td width="230"><span class="Estilo1"><?= $nombre; ?></span></td>
          <td width="260">&nbsp;</td>
          <td width="230" align="right"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
        </tr>
        <tr>
          <td><span class="Estilo1">R.U.C. : <?= $ruc; ?></span></td>
          <td align="center"><span class="Estilo1">LIBRO MAYOR </span></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td align="center"><span class="Estilo1"><?= 'Del mes '.$mes.' AL Mes de '.$mes; ?></span> <br /><span class="Estilo1">De la Cta. 10 a la Cta. 9570904</span></td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table  border="0">
        <tr>
          <td colspan="10"><hr /></td>
        </tr>
        <tr align="center">
          <td width="60"><span class="Estilo13">Fecha Reg. </span></td>
          <td width="60"><span class="Estilo13">Asiento </span></td>
          <td width="90"><span class="Estilo13">Nro. Documto. </span></td>
          <td width="70"><span class="Estilo13">Doc. Referencia </span></td>
          <td width="90"><span class="Estilo13">Detalle </span></td>
          <td width="50"><span class="Estilo13">T.C. </span></td>
          <td width="70"><span class="Estilo13">DEBE US$ </span></td>
          <td width="70"><span class="Estilo13">HABER US$ </span></td>
          <td width="70"><span class="Estilo13"> DEBE S/ </span></td>
          <td width="70"><span class="Estilo13">HABER S/ </span></td>
        </tr>

        <?php
        $sqlgral="SELECT distinct(asid_cuentad) cuentad,sp.plade_nombre,DATE_FORMAT(ag.asig_fecha,'%d/%m/%Y') as fechaasig,asig_moneda FROM sys_conta_asientos_general ag,sys_conta_asientos_detalle sd, sys_conta_plan_detalle sp
      	WHERE ag.asig_cod = sd.asid_cod and sd.asid_cuentad = sp.plade_codrela  and ag.emp_id = '$emp' AND EXTRACT(YEAR FROM ag.asig_fecha)='$anual'
        AND extract(MONTH FROM ag.asig_fecha)='$xmes'
      	ORDER BY asid_cuentad";
        $rgral=mysqli_query($con,$sqlgral);
        while($row=mysqli_fetch_array($rgral,MYSQLI_ASSOC)){
          $idcuentad = $row['cuentad'];
          $nocuentad = $row['plade_nombre'];
          $fechaasig = $row['fechaasig'];

          $sqlsaldos="SELECT SUM(asid_debe) tsdebe,SUM(asid_haber) tshaber FROM sys_conta_asientos_detalle sd, sys_conta_asientos_general ag
            WHERE ag.asig_cod = sd.asid_cod  AND asig_moneda='S' AND extract(MONTH FROM ag.asig_fecha)='$xmes' AND extract(YEAR FROM ag.asig_fecha)='$anual2' AND
            asid_cuentad='$idcuentad'";
            $rsqlsaldos = mysqli_query($con,$sqlsaldos);
            $asaldos = mysqli_fetch_array($rsqlsaldos,MYSQLI_ASSOC);
            $tshaber = $asaldos['tshaber']; $tsdebe = $asaldos['tsdebe'];
            $sqlsaldod="SELECT SUM(asid_debe) tddebe,SUM(asid_haber) tdhaber FROM sys_conta_asientos_detalle sd, sys_conta_asientos_general ag
              WHERE ag.asig_cod = sd.asid_cod  AND asig_moneda='USD' AND extract(MONTH FROM ag.asig_fecha)='$xmes' AND extract(YEAR FROM ag.asig_fecha)='$anual2' AND
              asid_cuentad='$idcuentad'";
              $rsqlsaldod = mysqli_query($con,$sqlsaldod);
              $asaldod = mysqli_fetch_array($rsqlsaldod,MYSQLI_ASSOC);
              $tdhaber = $asaldod['tdhaber']; $tddebe = $asaldod['tddebe'];

        ?>
        <tr><td colspan="10">--------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td></tr>
        <tr>
          <td colspan="2" align="left"><span class="Estilo13"><?=$idcuentad;?></span></td>
          <td colspan="2" align="left"><span class="Estilo13"><?=$nocuentad;?></span></td>
          <td align="right" colspan="2"><span class="Estilo13">SALDO AL MES <?=$mes_ant;?>:</span></td>
          <td align="center"><span class="Estilo13"><?= number_format($tddebe, 2,",",".");?></span></td>
          <td align="center"><span class="Estilo13"><?= number_format($tdhaber, 2,",",".");?></span></td>
          <td align="center"><span class="Estilo13"><?= number_format($tsdebe, 2,",",".");?></span></td>
          <td align="center"><span class="Estilo13"><?= number_format($tshaber, 2,",",".");?></span></td>
        </tr>
        <?php $sqldetalle ="SELECT DISTINCT(asid_cuentad),asid_cod from sys_conta_asientos_detalle where asid_cuentad='$idcuentad' and  asid_estatus='C'";
        $rdetalle=mysqli_query($con,$sqldetalle);
        while($row2=mysqli_fetch_array($rdetalle,MYSQLI_ASSOC)){ ?>
        <tr>
          <td  align="center" ><span class="Estilo13"><?=$fechaasig;?></span></td>
          <td align="center" ><span class="Estilo13"><?php
          $idasi = $row2['asid_cod'];
          $sqlgen = "SELECT asig_id,asig_ndoc,asig_tasa,asig_libro,asig_moneda FROM sys_conta_asientos_general WHERE asig_cod ='$idasi'";
          $rgen = mysqli_query($con,$sqlgen);
          $agen = mysqli_fetch_array($rgen,MYSQLI_ASSOC);
          $codasi = $agen['asig_id']; $numfac = $agen['asig_ndoc']; $tasag=$agen['asig_tasa']; $codlib=$agen['asig_libro']; $moneda=$agen['asig_moneda'];
          echo '0'.$codlib.' '.$codasi;
          ?></span></td>
          <td ><span class="Estilo13"><?=$numfac;?></span></td>
          <td >&nbsp;</td>
          <td ><span class="Estilo13"><?='POR'.' '.$numfac;?></span></td>
          <td  align="center"><span class="Estilo13"><?=$tasag;?></span></td>
          <td ><span class="Estilo13"><?php
          $sqltdebe = "SELECT sum(asid_debe) as tdbe FROM sys_conta_asientos_detalle WHERE asid_cod= '$idasi' AND asid_cuentad='$idcuentad' AND asid_estatus ='C' ";
          $rdebe=mysqli_query($con,$sqltdebe);
          $debsql=mysqli_fetch_array($rdebe,MYSQLI_ASSOC);
          $tdbe=$debsql['tdbe'];
          if ($moneda=='S') {
            $tdbes=$tdbe;
          } else {
            $tdbed=$tdbe;
            echo number_format($tdbed, 2,",",".");
          }
           ?></span></td>
          <td align="center"><span class="Estilo13">
            <?php
            $sqlthaber = "SELECT sum(asid_haber) as tdha FROM sys_conta_asientos_detalle WHERE asid_cod= '$idasi' AND asid_cuentad='$idcuentad' AND asid_estatus ='C' ";
            $rhaber=mysqli_query($con,$sqlthaber);
            $habesql=mysqli_fetch_array($rhaber,MYSQLI_ASSOC);
            $tdha=$habesql['tdha'];
            if ($moneda=='S') {
              $tdhas=$tdha;
              $totalhs = $totalhs + $tdhas;
            } else {
              $tdhad=$tdha;
              $totalhd = $totalhd + $tdhad;
              echo number_format($tdhad, 2,",",".");
            }
             ?></span></td>
          <td align="center" ><span class="Estilo13"><?php if ($tdbes>'0'): echo number_format($tdbes, 2,",",".");  else: echo '0.00'; endif; ?></span></td>
          <td align="center" ><span class="Estilo13"><?php if ($tdhas>'0'): echo number_format($tdhas, 2,",",".");  else: echo '0.00';endif;?></span></td>
        </tr>

      <?php } ?>
      <tr>
        <td align="right" colspan="6" ><span class="Estilo13">TOTAL MOVIMIENTO:</span></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td align="right" colspan="6" ><span class="Estilo13">SALDO PERIODO REPORTE:</span></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td align="right" colspan="6" ><span class="Estilo13">ACUMULADO A <?=$mes_ant;?>:</span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tddebe, 2,",",".");?></span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tdhaber, 2,",",".");?></span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tsdebe, 2,",",".");?></span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tshaber, 2,",",".");?></span></td>
      </tr>
      <tr>
        <td align="right" colspan="6" ><span class="Estilo13">SALDO A <?=$mes_ant;?>:</span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tddebe, 2,",",".");?></span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tdhaber, 2,",",".");?></span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tsdebe, 2,",",".");?></span></td>
        <td align="center"><span class="Estilo13"><?= number_format($tshaber, 2,",",".");?></span></td>
      </tr>
    <?php }?>
      </table>
</page_header>




<?php  } ?>
</page>

<?php
error_reporting(E_ERROR | E_PARSE);
include "../../funciones/BD.php";
$empid = $_GET['txtemp']; $xanual = $_GET['txtanual']; $xmes = $_GET['txtmes'];
$sqlemp = "SELECT emp_ruc,emp_nombre,Nmes('$xmes') as nmes FROM sys_empresas WHERE emp_id='$empid'";
$remp = mysqli_query($con,$sqlemp);
$aemp = mysqli_fetch_array($remp,MYSQLI_ASSOC);
$emp_nom = $aemp['emp_nombre'];
$emp_ruc = $aemp['emp_ruc'];
$mes_nom = $aemp['nmes'];

?>
<style>
  .p1 {
    line-height: 200%;
    text-align: justify;
  }
  .Estilo0 {font-size: 12px; font-weight: bold; }
  .Estilo1 {font-size: 11px; font-weight: bold; }
  .Estilo01 {font-size: 10px; font-weight: bold; }
  .Estilo11 {font-size: 10px; font-weight: bold; color: white;}
  .Estilo13 {font-size: 8px; font-weight: bold; }
  .tborde {
    border: 1.1px solid black;
  }
</style>
<page orientation="landscape" style="font-size: 7px" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
    <page_header>
      <table  border="0">
        <tr>
          <td width="650"><span class="Estilo0"><?=$emp_nom;?></span></td>
          <td width="191">&nbsp;</td>
          <td width="199" align="right"><em>P&aacute;gina: </em><strong>[[page_cu]]</strong></td>
        </tr>
        <tr>
          <td><span class="Estilo0">RUC: <?=$emp_ruc;?></span></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" align="center"><span class="Estilo0">VENTA <?=$mes_nom;?> - <?=$xanual;?> </span></td>
        </tr>
      </table>
      <table  border="0">
        <tr>
          <td width="70" rowspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">FECHA</span></td>
          <td colspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">VENTA </span></td>
          <td width="47" rowspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">T.C.</span></td>
          <td width="60" rowspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">S./ T.C</span></td>
          <td width="96" rowspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">TOTAL DIARIO </span></td>
          <td width="134" rowspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">ACUMULADO </span></td>
          <td width="409" rowspan="2" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">OBSERVACIONES </span></td>
        </tr>
        <tr>
          <td width="94" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">US$</span></td>
          <td width="94" align="center" class="tborde" style="background-color:#CCC;"><span class="Estilo01">S/.</span></td>
        </tr>
        <tr>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
          <td align="center" class="tborde">&nbsp;</td>
      </tr>
      </table>
    </page_header>
</page>

<?php
// Iniciar sesión
   error_reporting(E_ERROR | E_PARSE);
   session_start();
   header ("Cache-Control: private");
  // Si se ha enviado el formulario
   $usu_nomusu = $_REQUEST['usu_nomusu'];
   $usu_pass = $_REQUEST['usu_pass'];

   if (isset($usu_nomusu) && isset($usu_pass))
   {
   // Comprobar que el usuario está autorizado a entrar
     include "Funciones/BD.php";
     $salt = substr ($usu_nomusu, 0, 2);
     $pass_crypt = crypt ($usu_pass, $salt);

     $sql = "SELECT su.usu_id,su.usu_iden,su.usu_nombres_apellido,sg.gru_nombre,sg.gru_id,su.usu_nomusu,su.usu_password,su.usu_estatus FROM
			sys_adm_usuarios su, sys_adm_grupos sg WHERE su.gru_id=sg.gru_id AND usu_nomusu='$usu_nomusu' AND usu_password='$pass_crypt'";
  	 $result=mysqli_query($con,$sql);
     $nfilas = mysqli_num_rows($result);


    if ($nfilas > 0) {
      // Los datos introducidos son correctos
        $v_usuario = mysqli_fetch_array($result,MYSQLI_ASSOC);
		    mysqli_free_result($result);
       	$usuario_valido = $usu_nomusu;
		    $_SESSION["usuario_valido"] = $v_usuario['usu_id'];
        $_SESSION["usu_iden"] = $v_usuario['usu_iden'];
		    $_SESSION["gru_id"] = $v_usuario['gru_id'];
        $_SESSION["usu_estatus"] = $v_usuario['usu_estatus'];
        $_SESSION["usu_nombres_apellido"] = $v_usuario['usu_nombres_apellido'];
        $_SESSION["usu_id"] = $v_usuario['usu_id'];
      }
    }
      #REDIRECCION
     if (isset($_SESSION["usuario_valido"]) AND ($_SESSION["usu_estatus"]='1'))
     {
        if ($v_usuario['gru_id']=='1') {
            #SESION ADMINITRADOR
            header("Location:/GrupoLodi/Administrador/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='2')){
            #PERSONAL CONTADOR
            header("Location:/GrupoLodi/Contabilidad/index.php?menu=con");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='3')){
            #PERSONAL VENDEDOR - COTIZACIONES
            header("Location:/GrupoLodi/Ventas/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='4')){
            #PERSONAL - FACTURACION
            header("Location:/GrupoLodi/Facturacion/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='5')){
            #PERSONAL - IMPORTACIONES
            header("Location:/GrupoLodi/Importaciones/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='6')){
            #PERSONAL - COBRANZAS
            header("Location:/GrupoLodi/Cobranzas/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='7')){
            #PERSONAL - COSTOS
            header("Location:/GrupoLodi/Costos/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='8')){
            #PERSONAL - ALMACEN
            header("Location:/GrupoLodi/Almacen/");
        }elseif(isset($_SESSION["usuario_valido"]) AND ($_SESSION["gru_id"] =='9')){
            #PERSONAL - SUPERVISOR
            header("Location:/GrupoLodi/Supervisor/");
        }else {
            #ERROR - SESSION
            $error='-1';
        }
      }
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>ERP - GrupoLodi</title>

<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>

<body>
  <!-- Top content -->
  <div class="top-content">
    <div class="inner-bg">
      <div class="container">
        <div align="center" class="col-lg-12">
          <img src="assets/images/logo.png" class="img-responsive" width="410px" />
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12">
          <div class="col-md-6 col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
              <div class="form-top-left">
                <h3>Login</h3>
                  <p>Por favor ingrese su usuario y contrase&ntilde;a:</p>
              </div>
              <div class="form-top-right"><i class="fa fa-lock"></i></div>
            </div>
            <div class="form-bottom">
              <form role="form" action="" method="post" class="login-form">
                  <div class="form-group inputwrapper animate1 bounceIn">
                  <label class="sr-only" for="form-username">Usuario</label>
                    <input type="text" name="usu_nomusu" placeholder="Usuario..." class="form-username form-control" id="username">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-password">Contrase&ntilde;a</label>
                    <input type="password" name="usu_pass" placeholder="Contraseña..." class="form-password form-control" id="password">
                  </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Ingresar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Javascript -->
  <script src="assets/js/jquery-1.11.1.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.backstretch.min.js"></script>
  <script src="assets/js/scripts.js"></script>

  <!--[if lt IE 10]>
      <script src="assets/js/placeholder.js"></script>
  <![endif]-->


</body>
</html>

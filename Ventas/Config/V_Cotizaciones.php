<?php
  include "../Funciones/BD.php";

  /*if (iseet($_POST['submitSave'])) {
    if ($_POST['submitSave'] != '') {
      #agregar tc comercial
      $sql2="insert into sys_empresas values ('0','$xiden',UPPER('$xnom'),$xtel,UPPER('$xdire'),'$archivo','1')";
     if (!mysqli_query ($con,$sql2)) {
     echo("Error description: " . mysqli_error($con)); }
    }
  }*/
 ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="panel panel-default">
              <div class="panel-body">
              <div class="col-lg-12">
                <div class="row" id="titulo">
                      <div class="col-lg-6">
                      <h2 class="azul"><i class="fa fa-folder-open-o fa-fw"></i><strong>Cotizaciones Pendientes</strong></h2>
                    </div>
                      <div class="col-lg-6 text-right" >
                        	<button class="btn btn-primary" id="show"><i class="fa fa-plus "></i> Nueva Cotizaci&oacute;n</button>
                      </div>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="row">
                  <div id="titulo2" class="col-lg-6" style="display: none;" >
                    <h2 class="azul"><i class="fa fa-folder-open-o fa-fw"></i><strong>Nueva Cotizaci&oacute;n</strong></h2>
                  </div>
                </div>
              </div>
            <div class="table-responsive col-lg-12"><hr class="black" />
              <div  id="element2">
               <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                 <thead>
                   <tr>
                     <th class="text-center">#</th>
                     <th class="text-center">TIPO DE DOCUMENTO</th>
                     <th class="text-center">NUMERO</th>
                     <th class="text-center">RAZON SOCIAL</th>
                     <th class="text-center">FORMA DE PAGO</th>
                     <th class="text-center">MONEDA</th>
                     <th class="text-center">T.C.</th>
                     <th class="text-center">FECHA</th>
                     <th class="text-center">ESTATUS</th>
                     <th class="text-center">DETALLE</th>
                   </tr>
                 </thead>
                 <tbody class="text-center">
                 <?php
                 $sql="SELECT sc.car_id,sc.car_nombre, sc.car_estatus,sa.area_nombre from sys_rh_cargos sc,sys_rh_area sa WHERE
                    (sc.car_id>0 and length(sc.car_id)>0) and sa.area_id = sc.area_id ORDER BY sc.car_id Limit 2";
                 $result=mysqli_query($con,$sql);

                 while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
                   <tr>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                     <!--EDITAR-->
                       <td class="centeralign">
                           <form  id="EditarE" role="form" action="index.php?menuu=3" method="post">
                                <input type="hidden" name="opcion" value="act">
                             <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?php echo $row['car_id']; ?>'></button>
                           </form>
                       </td>

                   </tr>
                     <?php } ?>
                 </tbody>
               </table>
               </div>
             </div>
               <!--Div Oculto-->
               <div id="content">
                  <div id="element" style="display: none;">
                    <form  id="RegistroC" role="form" action="index.php?menuu=" method="post">


                     <div class="col-lg-4 col-md-4 hidden-xs top">
                      <button type="submit" class="btn btn-primary" name="Guardar" value="Insertar">Guardar </button>
                      <button type="reset" class="btn btn-default">Limpiar</button>
                      <a href="#" class="btn btn-warning" id="hide"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    </div>
                    <div class="col-xs-12 visible-xs top">
                      <button type="submit" class="btn btn-primary" name="Guardar" value="Insertar">Guardar </button>
                      <button type="reset" class="btn btn-default">Limpiar</button>
                      <a href="#" class="btn btn-warning" id="hide"><i class="glyphicon glyphicon-chevron-left"></i></a>
                   </div>
                  </form>
                  </div>
                </div>

      </div>
    </div>
</div>
        <!-- /.col-lg-12 -->
</div>

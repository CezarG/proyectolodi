
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog info">
    <form action="index.php" method="post">

      <div class="modal-content">
          <div class="modal-header">
              <h3 align="center" class="azul" id="myModalLabel"><i class="fa fa-bars fa-fw"></i>&nbsp;<strong>Agregar T.C</strong></h3>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label><strong>NUEVA T.C.:</strong></label>
              <input class="form-control" placeholder="Ej: 3.330" pattern="\d+(\.\d{3})?" title="0.000" name="tcomercial" id="tcomercial"  required>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Cerrar</button>
              <button type="submit" name="submitSave" value="submitSave" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar</button>
          </div>
        </form>
      </div>
  </div>
</div>

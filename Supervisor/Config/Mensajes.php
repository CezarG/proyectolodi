<?php
#GUARDAR
$m_gua ='<hr class="successline" />
 <div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row" id="titulo">
          <div class="col-lg-12">
            <div class="col-lg-12 text-center">
               <h2 class="black">  <i class="glyphicon glyphicon-ok suces"></i>&nbsp;<strong>GUARDANDO LOS DATOS</strong></h2>
            </div>
           </div>
         </div>
         <div class="table-responsive col-lg-12"><hr class="successline" />
           <div class="progress">
             <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
       100%
             </div>
           </div>
        </div>
       </div>
     </div>
   </div>
 </div>';
#Deshabilitar
$m_des ='<hr class="dangerline" />
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12">
  <div class="panel panel-danger">
    <div class="panel-body">
      <div class="row" id="titulo">
        <div class="col-lg-12">
          <div class="col-lg-12 text-center">
             <h2 class="black"><i class="glyphicon glyphicon-remove dange"></i>&nbsp;<strong>DESHABILITANDO LOS DATOS</strong></h2>
          </div>
         </div>
       </div>
       <div class="table-responsive col-lg-12"><hr class="dangerline" />
         <div class="progress">
           <div class="progress-bar progress-bar-striped progress-bar-danger active" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
     100%
           </div>
         </div>
      </div>
     </div>
   </div>
 </div>
</div>';
#Habilitar
$m_hab='<hr class="successline" />
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="row" id="titulo">
        <div class="col-lg-12">
          <div class="col-lg-12 text-center">
             <h2 class="black">  <i class="glyphicon glyphicon-ok suces"></i>&nbsp;<strong>HABILITANDO LOS DATOS</strong></h2>
          </div>
         </div>
       </div>
       <div class="table-responsive col-lg-12"><hr class="successline" />
         <div class="progress">
           <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
     100%
           </div>
         </div>
      </div>
     </div>
   </div>
 </div>
</div>';
#Agregar
$m_agg ='<hr class="successline" />
   <div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row" id="titulo">
            <div class="col-lg-12">
              <div class="col-lg-12 text-center">
                 <h2 class="black">  <i class="glyphicon glyphicon-ok suces"></i>&nbsp;<strong>AGREGANDO LOS DATOS</strong></h2>
              </div>
             </div>
           </div>
           <div class="table-responsive col-lg-12"><hr class="successline" />
             <div class="progress">
               <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
         100%
               </div>
             </div>
          </div>
         </div>
       </div>
     </div>
   </div>';
#Actualizar
$m_act ='<hr class="successline" />
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="row" id="titulo">
        <div class="col-lg-12">
          <div class="col-lg-12 text-center">
             <h2 class="black">  <i class="glyphicon glyphicon-ok suces"></i>&nbsp;<strong>ACTUALIZANDO LOS DATOS</strong></h2>
          </div>
         </div>
       </div>
       <div class="table-responsive col-lg-12"><hr class="successline" />
         <div class="progress">
           <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
     100%
           </div>
         </div>
      </div>
     </div>
   </div>
 </div>
</div>';
#error
$m_error = '<hr class="dangerline" />
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12">
  <div class="panel panel-danger">
    <div class="panel-body">
      <div class="row" id="titulo">
        <div class="col-lg-12">
          <div class="col-lg-12 text-center">
             <h2 class="black"><i class="glyphicon glyphicon-remove dange"></i>&nbsp;<strong>EROR EN LOS DATOS</strong></h2>
          </div>
         </div>
       </div>
       <div class="table-responsive col-lg-12"><hr class="dangerline" />
         <div class="progress">
           <div class="progress-bar progress-bar-striped progress-bar-danger active" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
     100%
           </div>
         </div>
      </div>
     </div>
   </div>
 </div>
</div>';


?>

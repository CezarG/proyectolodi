<?php
  include "../Funciones/BD.php";
  require_once('M_Perfil.php');
  require_once('M_Pagos.php');
  $anual=date("Y");
  $subt1='Agencias'; $lnk1='index.php?menu=6&opc=consul'; $iconn1='fa fa fa-truck';
  $subt2='Empresas'; $lnk2='index.php?menu=7';  $iconn2='fa fa-building-o';
  $subt3='Marcas'; $lnk3='index.php?menu=14'; $iconn3='fa fa-list';
  $subt4='Productos'; $lnk4='index.php?menu=18&opc=acc';   $iconn4='fa fa-asterisk';
  $subt5='Proveedores'; $lnk5='index.php?menu=19&opc=consul'; $iconn5='fa fa-group';
  $subt6='Importaci&oacute;n'; $lnk6='index.php?menu=1'; $iconn6='fa fa-exchange';
  #$subt7='Usuarios'; $lnk7='';$iconn7='fa fa-cogs fa-5x';
  $subt8='Perfil'; $lnk8='';   $iconn8='fa fa-user';


 ?>
<div class="panel panel-default">
  <div class="panel-body">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <a href="<?php echo $lnk1; ?>">
      <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
          <div class="inner">
            <p><?=$subt1;?></p>
          </div>
          <div class="icon">
            <i class="<?=$iconn1;?>" style="color:#fff;"></i>
          </div>
          <div class="small-box-footer">M&aacute;s Informaci&oacute;n <i class="fa fa-arrow-circle-right"></i></div>
        </div>
      </div></a>
      <a href="<?php echo $lnk2; ?>">
      <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <p><?php echo $subt2; ?>  </p>
          </div>
          <div class="icon">
            <i class="<?=$iconn2;?>" style="color:#fff;"></i>
          </div>
          <div class="small-box-footer">M&aacute;s Informaci&oacute;n <i class="fa fa-arrow-circle-right"></i></div>
        </div>
      </div></a>
      <!-- ./col -->
      <a href="<?php echo $lnk3; ?>">
      <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">

            <p><?php echo $subt3; ?>  </p>
          </div>
          <div class="icon">
            <i class="<?=$iconn3;?>" style="color:#fff;"></i>
          </div>
          <div class="small-box-footer">M&aacute;s Informaci&oacute;n <i class="fa fa-arrow-circle-right"></i></div>
      </div></div></a>
      <!-- ./col -->
      <a href="<?php echo $lnk4; ?>">
      <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <p><?php echo $subt4; ?> </p>
          </div>
          <div class="icon">
            <i class="<?=$iconn4;?>" style="color:#fff;"></i>
          </div>
          <div class="small-box-footer">M&aacute;s Informaci&oacute;n <i class="fa fa-arrow-circle-right"></i></div>
      </div></div></a>
      <!-- ./col -->
      <a href="<?php echo $lnk6; ?>">
      <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <p><?=$subt6;?></p>
          </div>
          <div class="icon">
            <i class="<?=$iconn6;?>" style="color:#fff;"></i>
          </div>
          <div class="small-box-footer">M&aacute;s Informaci&oacute;n <i class="fa fa-arrow-circle-right"></i></div>
        </div>
      </div></a>
      <a href="#M_Perfil" data-toggle="modal">
      <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-purple-gradient">
          <div class="inner">
            <p><?=$subt8;?></p>
          </div>
          <div class="icon">
            <i class="<?=$iconn8;?>" style="color:#fff;"></i>
          </div>
          <div class="small-box-footer">M&aacute;s Informaci&oacute;n <i class="fa fa-arrow-circle-right"></i></div>
        </div>
      </div>
      </a>
      <!-- ./col -->
    </div>
    <div class="row">
      <div class="col-lg-9">
        <div class="callout callout-success">
         <h4 align="center"><i class="fa fa-exchange fa-fw"></i> <strong>Importaciones -  Fechas / Pagos</strong></h4>
       </div>
        <div class="box box-success">
                    <div class="box-body">
                      <table id="dataTables-example" class="table table-bordered table-striped ">
                        <thead>
                        <tr class="bg-green-gradient">
                          <th class="text-center">#</th>
                          <th class="text-center">FOLDER</th>
                          <th class="text-center">IMPORTADOR</th>
                          <th class="text-center">PROVEEDOR</th>
                          <th class="text-center">ETA</th>
                          <th class="text-center">LIBRE DE SOBREESTADIA</th>
                          <th class="text-center">Pagos</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <tr>
                          <td>1</td>
                          <td>2020-54</td>
                          <td>GRUPO LODI SRL</td>
                          <td>BOE COMMERCE</td>
                          <td>15/06/2020</td>
                          <td>04/07/2020</td>
                          <td><button class="btn btn-primary btn-sm glyphicon glyphicon-list" disabled></button></td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>2020-57</td>
                          <td>GRUPO LODI SRL</td>
                          <td>TORNEL</td>
                          <td>22/06/2020</td>
                          <td>11/06/2020</td>
                          <td>
                            <form   role="form" action="index.php?" method="get">
                              <input type="hidden" name="opc" value="not">
                              <button class="btn btn-primary btn-sm glyphicon glyphicon-list"></button>
                            </form>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box --></div>
      <div class="col-lg-3 col-xs-6">
      <!-- small box -->
        <div class="small-box bg-light-blue-gradient">
          <div class="calendar calendar-first" id="calendar_first">
            <div class="calendar_header">
              <button class="switch-month switch-left"> <i class="fa fa-chevron-left"></i></button>
              <h2><i class="fa fa-calendar"></i></h2>
              <button class="switch-month switch-right"> <i class="fa fa-chevron-right"></i></button>
            </div>
            <div class="calendar_weekdays"></div>
            <div class="calendar_content"></div>
          </div>
        </div>
      </div><!--col-->

    </div>
     <!--</div>-->
   </div> <!-- /.pbody -->
</div> <!-- /.pdefault -->

<?php
  include "../Funciones/BD.php";
  #Perfil de Usuario
  $sql_per="SELECT su.usu_id,su.usu_iden,su.usu_nombres_apellido,su.usu_nomusu,su.usu_password,su.gru_id, sg.gru_nombre
      FROM sys_adm_usuarios su, sys_adm_grupos sg WHERE usu_iden='$usuid' AND sg.gru_id = su.gru_id; ";
  $result_per=mysqli_query($con,$sql_per);
  $rsql_per=mysqli_fetch_array($result_per,MYSQLI_ASSOC);
  $xdni = $rsql_per['usu_iden'];
  $xnom = $rsql_per['usu_nombres_apellido'];
  $xgru = $rsql_per['gru_nombre'];
  $xusu = $rsql_per['usu_nomusu'];
  $xpass = $rsql_per['usu_password'];
  $id =  $rsql_per['usu_id'];
 ?>
 <style>
     .ancho {
       width: 600px;
     }
     .alto{
       height: 260px;
     }
 </style>
 <div class="modal fade" id="M_Perfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-user fa-fw"></i>&nbsp;<strong>Actualizar Perfil</strong></h3>
           </div>
           <div class="modal-body alto">
             <form  id="RegistroC" role="form" action="index.php?menu=0" method="post">
              <input type="hidden" name="id" value="<?php echo $id; ?>">
              <input type="hidden" name="xdni" value="<?php echo $xdni; ?>">
              <input type="hidden" name="xusu" value="<?php echo $xusu; ?>">
              <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label><strong>DNI:</strong></label>
                <input class="form-control" placeholder="000000000" name="xdni" value="<?php echo $xdni; ?>" disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-8">
                  <label><strong>NOMBRES Y APELLIDOS:</strong></label>
                  <input class="form-control text-uppercase" name="xnom" value="<?php echo $xnom; ?>"  disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>GRUPO:</strong></label>
                <select class="form-control"  name="xgru" disabled>
                  <?php echo '<option value="'.$xgru.'">'.$xgru.'</opcion>'; ?>
               </select>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-6">
                    <label><strong>USUARIO:</strong></label>
                    <input class="form-control" name="xusu" value="<?php echo $xusu; ?>"  disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-6">
                    <label><strong>CONTRASE&Ntilde;A ANTERIOR:</strong></label>
                    <input type="password" class="form-control" name="xpass2" value="<?php echo $xpass; ?>"  disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-6">
                    <label><strong>CLAVE NUEVA:</strong></label>
                    <input type="password" class="form-control" placeholder="Clave" name="xpass" pattern="[0-9.- ].{4,}" required>
              </div>

           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="actualiza" value="act"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>

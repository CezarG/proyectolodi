 <style>
     .pagoancho {
       width: 800px;
     }
     .pagoalto{
       height: 310px;
     }
 </style>
 <div class="modal fade" id="M_Pagos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog pagoancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-exchange fa-fw"></i>&nbsp;<strong>Importaciones - Pagos</strong></h3>
           </div>
           <div class="modal-body pagoalto">
            <div class="col-lg-12">
              <div class="alert alert-info" role="alert">
                        <strong>Nota:</strong> PAGAR AL PROVEEDOR ANTES DE LA LLEGADA.

                      </div></div>
             <form  id="RegistroC" role="form" action="index.php?menu=1" method="post">
              <input type="hidden" name="id" value="<?php echo $id; ?>">
              <input type="hidden" name="xdni" value="<?php echo $xdni; ?>">
              <input type="hidden" name="xusu" value="<?php echo $xusu; ?>">
              <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label><strong>FOLDER:</strong></label>
                <input class="form-control" placeholder="000000000" name="xdni"  disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-8">
                <label><strong>IMPORTADOR:</strong></label>
                <input class="form-control text-uppercase" name="xnom"   disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-8">
                <label><strong>PROVEEDOR:</strong></label>
                <input class="form-control text-uppercase" name="xnom"   disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label><strong>ETA:</strong></label>
                <input type="date" class="form-control text-uppercase" name="xnom"   disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label><strong>INCOTERM:</strong></label>
                <input class="form-control text-uppercase" name="xnom"   disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label><strong>MONTO:</strong></label>
                <input class="form-control text-uppercase" name="xnom"   disabled>
              </div>
              <div class="form-group col-xs-12 col-md-12 col-lg-4">
                <label><strong>SALDO:</strong></label>
                <input class="form-control text-uppercase" name="xnom"   disabled>
              </div>

           </div>
           <div class="modal-footer">
             <!--<button type="submit" class="btn btn-sm btn-primary" name="actualiza" value="act"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>-->
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
 <?php if ($_GET['opc'] !='' AND $_GET['opc'] =='not'): ?>
   <script type="text/javascript">
     $(document).ready(function()
     {
        $("#M_Pagos").modal("show");
     });
   </script>
 <?php endif; ?>

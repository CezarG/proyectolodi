<?php
  include "../Funciones/BD.php";
  if ($_GET['opc']=='acc') {
    include "Vistas/Modal/M_Accesorio.php";
    $title = "Accesorios";
  }
  if ($_GET['opc']=='aro') {
    include "Vistas/Modal/M_Aro.php";
    $title = "Aros";
  }
  if ($_GET['opc']=='cam') {
    include "Vistas/Modal/M_Camara.php";
    $title = "Camaras";
  }
  if ($_GET['opc']=='neu') {
    include "Vistas/Modal/M_Neumatico.php";
    $title = "Neumaticos";
  }
  if ($_GET['opc']=='pro') {
    include "Vistas/Modal/M_Protector.php";
    $title = "Protectores";
  }
 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-5">
              <h2 class="azul"><i class="fa fa-asterisk fa-fw"></i><strong>Productos: <?=$title?></strong></h2>
            </div>
            <div class="col-lg-7 text-right" >
              <?php if ($_GET['opc']=='acc'): ?>
              <a  class="btn btn-primary" href="#M_Accesorio" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <?php if ($_GET['opc']=='aro'): ?>
              <a  class="btn btn-primary" href="#M_Aro" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <?php if ($_GET['opc']=='cam'): ?>
              <a  class="btn btn-primary" href="#M_Camara" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <?php if ($_GET['opc']=='neu'): ?>
              <a  class="btn btn-primary" href="#M_Neumatico" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <?php if ($_GET['opc']=='pro'): ?>
              <a  class="btn btn-primary" href="#M_Protector" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
              <?php endif; ?>
              <a  class="btn btn-danger" href="index.php?menu=18&opc=acc"><i class="fa fa-bars"></i> Accesorios</a>
              <a  class="btn btn-default" href="index.php?menu=18&opc=aro"><i class="fa fa-bars"></i> Aros</a>
              <a  class="btn btn-success" href="index.php?menu=18&opc=cam"><i class="fa fa-bars"></i> Camaras</a>
              <a  class="btn btn-info" href="index.php?menu=18&opc=neu"><i class="fa fa-bars"></i> Neumaticos</a>
              <a  class="btn btn-warning" href="index.php?menu=18&opc=pro"><i class="fa fa-bars"></i> Protectores</a>
            </div>
          </div>
        </div>
        <div class="table-responsive col-lg-12"><hr class="black" />
           <table class="table table-striped table-bordered table-hover" id="dataTables-example">
             <thead>
               <tr>
                  <th class="text-center">SKU</th>
                 <?php if ($_GET['opc']=='acc'): ?>
                   <th class="text-center">MARCA</th>
                   <th class="text-center">DESCRIPCION</th>
                   <th class="text-center">PART NUMBER</th>

                  <?php endif; ?>
                  <?php if ($_GET['opc']=='aro'): ?>
                    <th class="text-center">MARCA</th>
                    <th class="text-center">TIPO</th>
                    <th class="text-center">MEDIDA</th>
                    <th class="text-center">ESPESOR</th>
                    <th class="text-center">N° HUECOS</th>
                    <th class="text-center">PAIS</th>
                  <?php endif; ?>
                  <th class="text-center">SUNAT</th>
                   <th class="text-center">EDITAR</th>

               </tr>
             </thead>
             <tbody class="text-center">
             <tr>
                <td>&nbsp;</td>
                  <?php if ($_GET['opc']=='acc'): ?>

                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <?php endif; ?>
                   <?php if ($_GET['opc']=='aro'): ?>

                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <?php endif; ?>
                   <td>&nbsp;</td>
                 <!--EDITAR-->
                   <td class="centeralign">
                     <form  id="EditarE" role="form" action="index.php?" method="get">
                          <input type="hidden" name="menuu" value="1">
                       <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?=$row['car_id']; ?>'></button>
                     </form>
                   </td>
               </tr>
             </tbody>
           </table>


        </div><!--col-lg-12-->
      </div><!---pbody-->
    </div>
  </div>
</div>

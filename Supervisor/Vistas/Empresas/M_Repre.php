<style>
     .M_Reprealto{
       height: 410px;
     }
 </style>
 <div class="modal fade" id="M_Repre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-user fa-fw"></i>&nbsp;<strong>Agregar Representate Legal</strong></h3>
           </div>
           <div class="modal-body M_Reprealto">
             <form  id="RegistroC" role="form" action="index.php?menu=10" enctype="multipart/form-data" method="post">
               <input name="action" type="hidden" value="upload" />
               <input type="hidden" name="form" value="repre">
                 <input type="hidden" name="agg" value="<?=$agreg;?>">
              <div class="form-group col-md-8 col-xs-12 col-lg-6">
                <label><strong>TIPO DE DOCUMENTO:</strong></label>
                <select class="form-control"  name="txttdo" required>
                  <?php
                    #TIPO DE DOCUMENTO
                    $sql2="SELECT doc_sunat_id ,(CASE WHEN doc_sunat_id = 6 THEN CONCAT('RUC')
                            WHEN doc_sunat_id = '0' THEN CONCAT('OTROS')
                            WHEN doc_sunat_id = '1' THEN CONCAT('DNI')
                            WHEN doc_sunat_id = '7' THEN CONCAT('PASAPORTE')
                            WHEN doc_sunat_id = '4' THEN CONCAT('CARNET DE EXTRANJERIA')
                            WHEN doc_sunat_id = 'A' THEN CONCAT('CEDULA DIPLOMATICA')
                            ELSE 'NT'
                            END) AS doc_nombre
               FROM sys_documentos WHERE doc_tipo='DE IDENTIDAD'
                   ORDER BY doc_sunat_id";
                    $rsql2=mysqli_query($con,$sql2);
                     echo "<option value=''>--</option>";
                     if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                     do{
                        echo '<option value="'.$row2['doc_sunat_id'].'">'.$row2['doc_nombre'].'</option>';
                        } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                     }
                   ?>
               </select>
              </div>
              <div class="form-group col-md-8 col-xs-12 col-lg-6">
                <label><strong>NUMERO:</strong></label>
                <input class="form-control text-uppercase" name="txtndo"  required>
              </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-7">
                 <label><strong>NOMBRES Y APELLIDOS:</strong></label>
                 <input class="form-control text-uppercase" name="txtnom" title=" Ingrese Letras y Numeros. Caracteres Admitidos(/*.-)" autofocus>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-5">
                 <label><strong>CARGO:</strong></label>
                 <select class="form-control" name="txtcar" required>
                   <option value="">--</option>
                   <option value="APODERADO">APODERADO</option>
                   <option value="DIRECTOR">DIRECTOR</option>
                   <option value="GERENTE">GERENTE</option>
                   <option value="CONTADOR">CONTADOR</option>
                 </select>
               </div>
               <div class="form-group col-md-12 col-xs-12 col-lg-12">
                 <img id="uploadPreview5" class="bordeazul" width="506" height="180" src="../Imagenes/Logos/noimage.png" />
                 <input height="20" class="form-control btn btn-primary" title="AGREGAR FIRMA" accept="image/png, image/jpeg, image/gif" id="uploadImage5" type="file" name="uploadImage5" onchange="previewImage(5);" />
                </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitRepre" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>
 <script>
 function previewImage(nb) {
     var reader = new FileReader();
     reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
     reader.onload = function (e) {
         document.getElementById('uploadPreview'+nb).src = e.target.result;
     };
 }
 </script>
 <?php if ($_GET['editr'] != ''): ?>
   <script type="text/javascript">
     $(document).ready(function()
     {
        $("#E_Repre").modal("show");
     });
   </script>
 <?php endif; ?>

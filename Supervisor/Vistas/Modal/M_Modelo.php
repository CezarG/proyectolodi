
 <style>
     .Modeloancho {
       width: 400px;
     }
     .Modeloalto{
       height: 170px;
     }
 </style>
<div class="modal fade" id="M_Modelo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Modeloancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Agregar Modelo</strong></h3>
           </div>

           <div class="modal-body Modeloalto">
             <div class="form-group col-xs-12 col-md-12 col-lg-12">
               <label><strong>MARCA:</strong></label>
               <input class="form-control text-uppercase" placeholder="MARCA" name="txtcate" disabled>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-12">
                 <label><strong>MODELO:</strong></label>
                 <input class="form-control text-uppercase" placeholder="Ingrese nombre de la marca" name="txtnmarca" required>
             </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>

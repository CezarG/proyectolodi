
 <style>
     .Polizancho {
       width: 400px;
     }
     .Polizalto{
       height: 170px;
     }
 </style>
<div class="modal fade" id="M_Poliza" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Polizancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Agregar P&oacute;liza</strong></h3>
           </div>

           <div class="modal-body Polizalto">
             <div class="form-group col-xs-12 col-md-12 col-lg-12">
               <label><strong>EMPRESA:</strong></label>
               <input class="form-control text-uppercase" placeholder="Categoria" name="txtcate" required>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-12">
                 <label><strong>NUMERO:</strong></label>
                 <input class="form-control text-uppercase" placeholder="Ingrese numero de poliza" name="txtnpoliza" required>
             </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>


 <style>
     .Accesorioancho {
       width: 600px;
     }
     .Accesorioalto{
       height: 320px;
     }
 </style>
<div class="modal fade" id="M_Accesorio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Accesorioancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-list fa-fw"></i>&nbsp;<strong>Agregar Accesorio</strong></h3>
           </div>

           <div class="modal-body Accesorioalto">
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
               <label><strong>SKU:</strong></label>
               <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-xs-12 col-md-12 col-lg-6">
                 <label><strong>CODIGO SUNAT:</strong></label>
                 <input class="form-control text-uppercase"  disabled>
             </div>
             <div class="form-group col-md-6 col-xs-12 col-lg-6">
                <label>MARCA :</label>
                <select class="form-control" name="xmar5" id="xmar5"  title="Ingrese Marca">
                <?php foreach ($dataMarcaAce as $marcaAce) { ?>
                  <option value="<?= $marcaAce->id ?>" ><?= $marcaAce->nombre ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-6">
                <label>PAIS :</label>
                <select class="form-control" title="Ingrese Nombre Pais" >
                    <!-- Leer los Clasificacion -->
                    <?php
                    $sql12 = "SELECT pais_id,pais_nombre from sys_pais WHERE pais_estatus=1 order by pais_id";
                    $rsql12 = mysqli_query($con, $sql12);
                    echo "<option value='0' selected>--</option>";
                    if ($row12 = mysqli_fetch_array($rsql12, MYSQLI_ASSOC)) {
                        do {
                            echo '<option value="' . $row12['pais_id'] . '">' . $row12['pais_nombre'] . '</option>';
                        } while ($row12 = mysqli_fetch_array($rsql12, MYSQLI_ASSOC));
                    }
                    ?>
                </select>
            </div>
              <div class="form-group col-xs-12 col-md-6 col-lg-12" title="Ingrese Descripcion">
                  <label>DESCRIPCION :</label>
                  <input class="form-control text-uppercase" name="xdesc2" id="xdesc2" onkeyup="PasarValor5();">
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-6">
                  <label> P/N :</label>
                  <input type="text" class="form-control text-uppercase" name="xpn" id="xpn" title="Ingrese solo el P/N " onkeyup="PasarValor5();">
              </div>
              <div class="form-group col-md-6 col-xs-12 col-lg-6">
                  <label>PARTIDA ARANCELARIA :</label>
                  <input type="text" class="form-control text-uppercase" id="ace_xparti" name="ace_xparti" title="Ingrese Partida Arancelaria">
              </div>
           </div><!--pbody-->
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitMarca" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>

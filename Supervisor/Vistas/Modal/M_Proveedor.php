
 <style>
     .Proveancho {
       width: 600px;
     }
     .Provealto{
       height: 320px;
     }
 </style>
<div class="modal fade" id="M_Proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Proveancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-group fa-fw"></i>&nbsp;<strong>Agregar Proveedor</strong></h3>
           </div>
           <div class="modal-body Provealto">
             <form  id="RegistroC" role="form" action="index.php?menu=19" method="post">
             <div class="form-group col-md-8 col-xs-12 col-lg-8">
               <label><strong>NOMBRE / RAZON SOCIAL:</strong></label>
               <input class="form-control text-uppercase" name="txtnom"  placeholder="Ingrese Razon Social" pattern="[A-z ]+.{5}" title="Ingrese solo letras" required>
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-4">
               <label><strong>TELEFONO 1:</strong></label>
               <input class="form-control text-uppercase" name="txttele1"  pattern="[0-9._%+-]{1,25}$" placeholder="+51000000000"  required>
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-4">
               <label><strong>TELEFONO 2:</strong></label>
               <input class="form-control text-uppercase" name="txttele2"  pattern="[0-9._%+-]{1,25}$" placeholder="+51000000000">
             </div>
             <div class="form-group col-md-8 col-xs-12 col-lg-8">
               <label><strong>CONTACTO:</strong></label>
               <input class="form-control text-uppercase" name="txtcontacto"  placeholder="Ingrese contacto" pattern="[A-z ]+.{5}" title="Ingrese solo letras" required>
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-6">
               <label><strong>EMAIL 1:</strong></label>
               <input class="form-control" name="txtemail1"  title="Ingrese correo electronico" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="ejemplo@gmail.com">
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-6">
               <label><strong>EMAIL 2:</strong></label>
               <input class="form-control" name="txtemail2"  title="Ingrese correo electronico" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="ejemplo@gmail.com">
             </div>
             <div class="form-group col-md-8 col-xs-12 col-lg-12">
               <label><strong>DIRECCION:</strong></label>
               <input class="form-control text-uppercase" name="txtdirec"  placeholder="Ingrese Direccion" pattern="^([a-zA-Z ])[a-zA-Z0-9-_–\. ]+{1,60}$" title="Ingrese solo letras" >
             </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitProvee" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>
       </div>

   </div>
 </div>

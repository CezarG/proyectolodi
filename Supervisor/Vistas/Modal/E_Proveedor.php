<?php if ($_GET['edit'] != ''):
  $idprove = $_GET['edit'];
  $sqlprove = "SELECT * FROM sys_com_proveedor WHERE prov_id='$idprove'";
  $rprove = mysqli_query($con,$sqlprove);
  $provv = mysqli_fetch_array($rprove,MYSQLI_ASSOC);
  $txtnom = $provv['prov_descripcion'];
  $txttele1= $provv['prov_telefono1'];
  $txttele2= $provv['prov_telefono2'];
  $txtcontacto = $provv['prov_contacto'];
  $txtemail1 = $provv['prov_email1'];
  $txtemail2 = $provv['prov_email2'];
  $txtdirec = $provv['prov_direccion'];
endif; ?>
 <style>
     .Proveancho {
       width: 600px;
     }
     .Provealto{
       height: 320px;
     }
 </style>
<div class="modal fade" id="E_Proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Proveancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-group fa-fw"></i>&nbsp;<strong>Editar Proveedor</strong></h3>
           </div>
           <div class="modal-body Provealto">
            <form  id="RegistroC" role="form" action="index.php?menu=19" method="post">
              <input type="hidden" name="edit" value="<?=$idprove;?>">
             <div class="form-group col-md-8 col-xs-12 col-lg-8">
               <label><strong>NOMBRE / RAZON SOCIAL:</strong></label>
               <input class="form-control text-uppercase" name="txtnom" value="<?=$txtnom;?>" placeholder="Ingrese Razon Social" pattern="[A-z ]+.{5}" title="Ingrese solo letras" required>
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-4">
               <label><strong>TELEFONO 1:</strong></label>
               <input class="form-control text-uppercase" name="txttele1" value="<?=$txttele1;?>" pattern="[0-9._%+-]{1,25}$" placeholder="+51000000000"  required>
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-4">
               <label><strong>TELEFONO 2:</strong></label>
               <input class="form-control text-uppercase" name="txttele2" value="<?=$txttele2;?>" pattern="[0-9._%+-]{1,25}$" placeholder="+51000000000"  >
             </div>
             <div class="form-group col-md-8 col-xs-12 col-lg-8">
               <label><strong>CONTACTO:</strong></label>
               <input class="form-control text-uppercase" name="txtcontacto" value="<?=$txtcontacto;?>" placeholder="Ingrese contacto" pattern="[A-z ]+.{5}" title="Ingrese solo letras" required>
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-6">
               <label><strong>EMAIL 1:</strong></label>
               <input class="form-control " name="txtemail1" value="<?=$txtemail1;?>" title="Ingrese correo electronico" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="ejemplo@gmail.com">
             </div>
             <div class="form-group col-md-4 col-xs-12 col-lg-6">
               <label><strong>EMAIL 2:</strong></label>
               <input class="form-control " name="txtemail2" value="<?=$txtemail2;?>" title="Ingrese correo electronico" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="ejemplo@gmail.com">
             </div>
             <div class="form-group col-md-8 col-xs-12 col-lg-12">
               <label><strong>DIRECCION:</strong></label>
               <input class="form-control text-uppercase" name="txtdirec" value="<?=$txtdirec;?>" placeholder="Ingrese Direccion" pattern="^([a-zA-Z ])[a-zA-Z0-9-_–\. ]+{1,60}$" title="Ingrese solo letras" >
             </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitProvee" value="Edit"><i class="glyphicon glyphicon-floppy-saved"></i> Actualizar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>
       </div>

   </div>
 </div>
 <?php if ($_GET['edit'] !=''): ?>
   <script type="text/javascript">
     $(document).ready(function()
     {
        $("#E_Proveedor").modal("show");
     });
   </script>
 <?php endif; ?>


 <style>
     .Paisancho {
       width: 400px;
     }
     .Paisalto{
       height: 110px;
     }
 </style>
<div class="modal fade" id="M_Pais" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog Paisancho">
       <div class="modal-content">
           <div class="modal-header modal-header-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
               <h3 align="center" id="myModalLabel"><i class="fa fa-bars fa-fw"></i>&nbsp;<strong>Agregar Pa&iacute;s</strong></h3>
           </div>
           <div class="modal-body Paisalto">
             <form  id="EditarC" role="form" action="index.php?menu=4" method="post">
             <div class="form-group col-xs-12 col-md-12 col-lg-12">
                 <label><strong>NOMBRE:</strong></label>
                 <input class="form-control text-uppercase" placeholder="Ingrese nombre del pais" name="txtnom" pattern="[A-z ]+.{3}" title="Ingrese solo letras" required>
             </div>
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-sm btn-primary" name="SubmitPais" value="Save"><i class="glyphicon glyphicon-floppy-saved"></i> Guardar </button>
             <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cerrar</button></form>
           </div>   </form>
       </div>

   </div>
 </div>

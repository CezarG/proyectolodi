<?php
  include "../Funciones/BD.php";
  include "Vistas/Modal/M_Proveedor.php";
  include "Vistas/Modal/E_Proveedor.php";
  include "Funciones/Proveedor/Proveedor.php";

 ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-6">
              <h2 class="azul"><i class="fa fa-group fa-fw"></i><strong>Proveedores</strong></h2>
            </div>
            <div class="col-lg-6 text-right" >
              <a  class="btn btn-primary" href="#M_Proveedor" data-toggle="modal"><i class="fa fa-plus"></i> Agregar</a>
            </div>
          </div>
        </div>
        <?php if ($_REQUEST['opc'] =='consul'): ?>
        <div class="table-responsive col-lg-12"><hr class="black" />
          <?=$rmsg;?>
           <table class="table table-striped table-bordered table-hover" id="dataTables-example">
             <thead>
               <tr>
                 <th class="text-center">#</th>
                 <th class="text-center">DESCRIPCION</th>
                 <th class="text-center">TELEFONO</th>
                 <th class="text-center">CONTACTO</th>
                 <th class="text-center">DIRECCION</th>
                 <th class="text-center">EDITAR</th>
               </tr>
             </thead>
             <tbody class="text-center">
               <?php $fil=1;
               while($row=mysqli_fetch_array($rtrans,MYSQLI_ASSOC)){ ?>
               <tr>
                 <td><?=$fil++?></td>
                 <td><?=$row['prov_descripcion'];?></td>
                 <td><?=$row['prov_telefono1'];?></td>
                 <td><?=$row['prov_contacto'];?></td>
                 <td><?=$row['prov_direccion'];?></td>
                 <td><form  id="EditarE" role="form" action="index.php?" method="get">
                    <input type="hidden" name="menu" value="19">
                    <input type="hidden" name="opc" value="consul">
                    <button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="edit" value='<?=$row['prov_id']; ?>'></button>
                 </form></td>
               </tr>
                       <?php } ?>
             </tbody>
           </table>
        </div>
            <?php endif; ?>
      </div>
    </div>
  </div>
</div>

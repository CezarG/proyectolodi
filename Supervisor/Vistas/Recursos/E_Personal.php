<?php
  include "../Funciones/BD.php";
  $edit=$_POST['edit'];
  #Config de Listas - Personal
  include "Funciones/Recursos/ListasPersonal.php";

 ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="panel panel-default">
              <div class="panel-body">
                <div class="row" id="titulo">
                  <div class="col-lg-12">
                   <div class="col-lg-6">
                     <?php if ($opci=='per') { ?>
                       <h2 class="azul"><i class="fa fa-inbox fa-fw"></i><strong>Editar Datos Personales</strong></h2>
                     <?php } ?>
                     <?php if ($opci=='lab') { ?>
                       <h2 class="azul"><i class="fa fa-inbox fa-fw"></i><strong>Editar Datos Laborales</strong></h2>
                     <?php } ?>
                     <?php if ($opci=='otrosp') { ?>
                       <h2 class="azul"><i class="fa fa-inbox fa-fw"></i><strong>Agregar Otras Remuneraciones</strong></h2>
                     <?php } ?>

                    </div>
                  </div>
                </div>
                <div class="col-lg-12"><hr class="black" /></div>
                <form  id="RegistroE" role="form" action="index.php?menu=12" enctype="multipart/form-data"  method="post">
                  <input name="action" type="hidden" value="upload" />
                  <input type="hidden" name="id" value="<?= $id; ?>">
                  <input type="hidden" name="nid" value="<?= $xnum; ?>">
                  <input type="hidden" name="empid" value="<?= $empid; ?>">
                  <input type="hidden" name="opcion" value="<?= $opcion; ?>">
                  <input type="hidden" name="foto" value="<?= $foto; ?>">
                  <?php if ($opci=='per') { ?>
                    <div class="upload_image col-lg-3">
                      <table width="260" height="320"  class="table-responsive">
                      <tr>
                      <?php if ($foto !=''): ?>
                        <td class="bordeazul" align="center"><img id="uploadPreview1" class="img-responsive" src="../Imagenes/Personal/<?=$foto;?>" /></td>
                      <?php endif; ?>
                      <?php if ($foto ==''): ?>
                        <td class="bordeazul" align="center"><img id="uploadPreview1" class="img-responsive" src="../Imagenes/Personal/sinf.png" /></td>
                      <?php endif; ?>
                      </tr>
                      <tr>
                        <td height="20" align="center"><input class="form-control btn btn-primary" title="AGREGAR FOTO" accept="image/png, image/jpeg, image/gif" id="uploadImage1" type="file" name="uploadImage1" onchange="previewImage(1);" /></td>
                      </tr>
                      </table>
                    </div>
                   <div class="form-group  col-md-8 col-xs-12 col-lg-2">
                       <label>TIPO DOCUMENTO:</label>
                       <select class="form-control"  name="tdoc" required>
                         <?php
                           #Tipo de documentos
                            $sql2="SELECT doc_sunat_id ,(CASE WHEN doc_sunat_id = 6 THEN 'RUC'
                                    WHEN doc_sunat_id = '0' THEN 'OTROS'
                                    WHEN doc_sunat_id = '1' THEN 'DNI'
                                    WHEN doc_sunat_id = '7' THEN 'PASAPORTE'
                                    WHEN doc_sunat_id = '4' THEN 'CARNET E.'
                                    WHEN doc_sunat_id = 'A' THEN 'CEDULA D.'
                                    ELSE 'NT'
                                    END) AS doc_nombre
                                    FROM sys_documentos WHERE doc_tipo='DE IDENTIDAD' AND doc_sunat_id='$tdoc'
	                                  UNION ALL
                                  SELECT doc_sunat_id ,(CASE WHEN doc_sunat_id = 6 THEN 'RUC'
                                    WHEN doc_sunat_id = '0' THEN 'OTROS'
                                    WHEN doc_sunat_id = '1' THEN 'DNI'
                                    WHEN doc_sunat_id = '7' THEN 'PASAPORTE'
                                    WHEN doc_sunat_id = '4' THEN 'CARNET E.'
                                    WHEN doc_sunat_id = 'A' THEN 'CEDULA D.'
                                    ELSE 'NT'
                                    END) AS doc_nombre
                                    FROM sys_documentos WHERE doc_tipo='DE IDENTIDAD' AND doc_sunat_id<>'$tdoc' ";
                            $rsql2=mysqli_query($con,$sql2);

                            if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                            do{
                               echo '<option value="'.$row2['doc_sunat_id'].'">'.$row2['doc_nombre'].'</option>';
                               } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                            }
                          ?>
                      </select>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-2">
                     <label>N&Uacute;MERO:</small></label>
                     <input class="form-control" placeholder="000000000" name="xnum" value="<?php echo  $xnum;?>" pattern="[0-9+].{6,}" title="Ingrese numeros" required>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-5">
                       <label>NOMBRES Y APELLIDOS:</label>
                       <input class="form-control text-uppercase" name="xnom" value="<?php echo $xnom; ?>"  pattern="[A-z0-9 ].{1,}" title="Ingrese solo letras" required>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-2">
                     <label>SEXO:</label>
                     <select class="form-control"  name="xsex" required>
                       <?=$list_sexo;?>
                     </select>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-2">
                       <label>FECHA DE NACIMIENTO:</label>
                       <input class="form-control input-group-sm" type="date"  name="fnac" id="fnac" value="<?php echo $fnac; ?>" required>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-2">
                     <label>ESTADO CIVIL:</label>
                     <select class="form-control"  name="xciv">
                      <?= $list_civ;?>
                     </select>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-1">
                     <label>Nº HIJOS:</label>
                     <input class="form-control" placeholder="0" name="nhij" pattern="[0-9]" value="<?php echo $nhij; ?>" required>
                   </div>

                   <div class="form-group col-xs-12 col-md-12 col-lg-2">
                      <label>TELEFONO:</label>
                      <input class="form-control" placeholder="+00-000000" name="xtel" value="<?= $xtel;  ?>" pattern="[0-9+].{6,}">
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-2">
                     <label>MOVIL:</label>
                     <input class="form-control" placeholder="+00-000000" name="xmov" value="<?= $xmov;?>" pattern="[0-9+].{6,}">
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-3">
                     <label>NACIONALIDAD:</label>
                     <select class="form-control"  name="xnaci" required>
                       <?= $list_naci;?>
                     </select>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-4">
                     <label>GRADO DE INST.:</label>
                     <select class="form-control"  name="gins" required>
                       <?= $list_gra;?>
                     </select>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-5">
                     <label>PROFESION / TITULO / OCUPACI&Oacute;N:</label>
                     <input class="form-control text-uppercase" placeholder="Ingrese grado de educativo" name="xocu" value="<?= $xocu;?>" pattern="[A-z0-9 ].{1,}" required>
                   </div>
                   <div class="form-group col-xs-12 col-md-12 col-lg-4">
                     <label>EMAIL:</label>
                     <input class="form-control" placeholder="Ingrese email" name="xmail" value="<?= $xmail;?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" >
                   </div>


                   <div class="form-group col-xs-12 col-md-12 col-lg-6">
                     <label>DIRECCI&Oacute;N:</label>
                     <input class="form-control text-uppercase" placeholder="ingrese Direccion o Referencia" value="<?php echo $xdir; ?>" name="xdir" pattern="[A-z0-9 ].{1,}" required>
                   </div>
                   <div class="col-lg-3 col-md-3 hidden-xs top">
                      <button type="submit" class="btn btn-primary" name="Editar" value="pers">Actualizar </button>
                      <button type="reset" class="btn btn-default">Limpiar</button>
                     <a href="index.php?menu=11" class="btn btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    </div>
                        <?php } if ($opci=='lab')  {?>
                          <div class="form-group  col-md-8 col-xs-12 col-lg-2">
                              <label>TIPO DOCUMENTO:</label>
                              <select class="form-control"  name="tdoc" disabled>
                                <?php
                                #Tipo de documentos
                                 $sql2="SELECT doc_sunat_id ,(CASE WHEN doc_sunat_id = 6 THEN 'RUC'
                                         WHEN doc_sunat_id = '0' THEN 'OTROS'
                                         WHEN doc_sunat_id = '1' THEN 'DNI'
                                         WHEN doc_sunat_id = '7' THEN 'PASAPORTE'
                                         WHEN doc_sunat_id = '4' THEN 'CARNET E.'
                                         WHEN doc_sunat_id = 'A' THEN 'CEDULA D.'
                                         ELSE 'NT'
                                         END) AS doc_nombre
                            FROM sys_documentos WHERE doc_tipo='DE IDENTIDAD' and doc_sunat_id = '$tdoc'
                                ORDER BY doc_sunat_id";
                                 $rsql2=mysqli_query($con,$sql2);

                                 if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                                 do{
                                    echo '<option value="'.$row2['doc_sunat_id'].'">'.$row2['doc_nombre'].'</option>';
                                    } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                                 }
                               ?>
                             </select>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>N&Uacute;MERO:</small></label>
                            <input class="form-control" placeholder="000000000" name="xnum" value="<?php echo  $xnum;?>" pattern="[0-9+].{6,}" title="Ingrese numeros" disabled>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-4">
                              <label>NOMBRES Y APELLIDOS:</label>
                              <input class="form-control text-uppercase" name="xnom" value="<?php echo $xnom; ?>"  pattern="[A-z0-9 ].{1,}" title="Ingrese solo letras" disabled>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-4">
                             <label>EMPRESA:</label>
                             <select class="form-control"  name="xemp" id="xemp" disabled>
                               <?php
                                  $sql2="SELECT * FROM sys_empresas WHERE emp_id ='$empid'";
                                  $rsql2=mysqli_query($con,$sql2);

                                  if( $row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC)     ){
                                  do{
                                     echo '<option value="'.$row2['emp_id'].'">'.$row2['emp_nombre'].'</option>';
                                     } while($row2=mysqli_fetch_array($rsql2,MYSQLI_ASSOC));
                                  }
                                ?>
                            </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-4">
                             <label>CARGO:</label>
                             <select class="form-control"  name="xcar" required>
                               <?php
                                 #CARGOS
                                  $sqlb="SELECT DISTINCT(car_nombre),car_id FROM sys_rh_cargos WHERE car_id ='$xcar_ant'
                                          UNION ALL
                                          SELECT DISTINCT(car_nombre),car_id FROM sys_rh_cargos	WHERE car_id <>'$xcar_ant'
                                          ";
                                  $rsqlb=mysqli_query($con,$sqlb);

                                  if( $rowb=mysqli_fetch_array($rsqlb,MYSQLI_ASSOC)     ){
                                  do{
                                     echo '<option value="'.$rowb['car_id'].'">'.$rowb['car_nombre'].'</option>';
                                   } while($rowb=mysqli_fetch_array($rsqlb,MYSQLI_ASSOC));
                                  }
                                ?>
                            </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-3">
                             <label>CATEGORIA:</label>
                             <select class="form-control"  name="tcate" required>
                              <?= $list_cat;?>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-3">
                             <label>ESTADO:</label>
                             <select class="form-control"  name="estad" required>
                              <?= $list_esta; ?>
                             </select>
                           </div>
                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                             <label>TIPO:</label>
                             <select class="form-control"  name="tiemp" required>
                               <?= $list_tipoe;?>
                             </select>
                           </div>
                          <div class="form-group col-md-8 col-xs-12 col-lg-2">
                            <label>AREA:</label>
                            <select class="form-control"  name="xarea" required>
                              <?php
                                #AREAS
                                 $sqla="SELECT area_id,area_nombre FROM sys_rh_area WHERE area_id = '$xarea_ant'
                                        UNION ALL
                                        SELECT area_id,area_nombre FROM sys_rh_area WHERE area_id <> '$xarea_ant'
                                        ORDER BY area_id";
                                 $rsqla=mysqli_query($con,$sqla);

                                 if( $rowa=mysqli_fetch_array($rsqla,MYSQLI_ASSOC)     ){
                                 do{
                                    echo '<option value="'.$rowa['area_id'].'">'.$rowa['area_nombre'].'</option>';
                                  } while($rowa=mysqli_fetch_array($rsqla,MYSQLI_ASSOC));
                                 }
                               ?>
                           </select>
                         </div>


                           <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>FECHA DE INGRESO:</label>
                            <input class="form-control input-group-sm" type="date"  name="fing" id="fing" value="<?php echo $fing; ?>" required>
                           </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>FECHA DE CESE:</label>
                            <input class="form-control input-group-sm" type="date"  name="ffin" id="ffin">
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>TIPO DE PLANILLA:</label>
                            <select class="form-control"  name="tpla" required>
                              <?=$list_pla;?>
                            </select>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>TIPO DE BOLETA:</label>
                            <select class="form-control"  name="tbol" required>
                            <?= $lis_bol;?>
                            </select>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>SUELDO (S/):</label>
                            <input class="form-control" placeholder="0.00" name="xsuel" value="<?php echo $xsuel; ?>" pattern="\d+(\.\d{2})?">
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>OTROS PAGOS (S/):</label>
                            <input class="form-control" placeholder="0.00" name="otrop" value="<?php echo $otrop; ?>" pattern="\d+(\.\d{2})?">
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>TIPO DE AFP:</label>
                           <select class="form-control"  name="xtfon" required>
                             <?= $list_tfon;?>
                           </select>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>FECHA DE INSCRIPCION:</label>
                            <input class="form-control input-group-sm" type="date"  name="finc" id="finc" value="<?php echo $finc; ?>" required>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                             <label>FONDO:</label>
                             <select class="form-control"  name="xfon" required>
                               <?= $list_afp;?>
                             </select>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>Nº CUSPP:</label>
                            <input class="form-control text-uppercase" placeholder="00000000" name="ncuspp" value="<?php echo  $ncuspp; ?>" pattern="[A-z0-9 ].{1,}" required>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>Nº CUENTA PERSONAL:</label>
                            <input class="form-control" placeholder="00000000" name="nper" value="<?php echo $nper; ?>" pattern="[0-9+].{6,}" required>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>Nº CUENTA CTS:</label>
                            <input class="form-control" placeholder="00000000" name="ncts" value="<?php echo $ncts; ?>" pattern="[0-9+].{6,}" required>
                          </div>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                            <label>ESTATUS:</label>
                            <select class="form-control"  name="xestatus" required>
                              <?php if ($xestatus_ant=='A'): ?>
                                <option value="A">ACTIVO</option>
                                <option value="I">INACTIVO</option>
                              <?php endif; ?>
                              <?php if ($xestatus_ant=='I'): ?>
                                <option value="I">INACTIVO</option>
                                <option value="A">ACTIVO</option>
                              <?php endif; ?>

                            </select>
                          </div>
                          <div class="col-lg-3 col-md-3 hidden-xs top">
                             <button type="submit" class="btn btn-primary" name="Editar" value="labs">Actualizar </button>
                             <button type="reset" class="btn btn-default">Limpiar</button>
                             <a href="index.php?menu=11" class="btn btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
                           </div>
                        <?php } ?>
                        </form>
                        <?php if ($opci=='otrosp') { ?>

                        <div class="col-lg-12">
                          <div class="alert alert-info">
                             <strong>NOTA:</strong> Recuerde registrar si el empleado recibe otras remuneraciones en otras entidades ya que estas son tomadas para el c&aacute;lculo de la planilla.
                           </div>
                         </div>
                         <?php if ($tip=='SI' or $tip=='NO') { ?>
                           <form role="form" method="get" action="pdf_documentos/documentos/declara_remunera.php" onSubmit = "return validar(this)"  target="noexiste">
                           <input type="hidden" name="id" value="<?php echo $xnum; ?>">
                           <input type="hidden" name="opcion" value="<?php echo $tip; ?>">
                           <input type="hidden" name="empid" value="<?php echo $id_emp; ?>">
                         <?php }  if ($tip==''){  ?>
                           <form id="RegistroP" role="form" method="get" action="index.php?">
                              <input type="hidden" name="menuu" value="6">
                              <input type="hidden" name="id" value="<?php echo $xnum; ?>">
                         <?php } ?>

                            <div class="form-group col-xs-12 col-md-12 col-lg-2">
                              <label>DOCUMENTO:</small></label>
                              <input class="form-control" placeholder="000000000" name="xnum" value="<?php echo  $todc.' - '.$xnum;?>" pattern="[0-9+].{6,}" title="Ingrese numeros" disabled>
                            </div>
                            <div class="form-group col-xs-12 col-md-12 col-lg-4">
                                <label>NOMBRES Y APELLIDOS:</label>
                                <input class="form-control text-uppercase" name="xnom" value="<?php echo $xnom; ?>"  pattern="[A-z0-9 ].{1,}" title="Ingrese solo letras" disabled>
                            </div>
                          <!--Configurar Reporte -->
                          <?php if ($tip=='') { ?>
                          <div class="form-group col-xs-12 col-md-12 col-lg-2">
                                <label>OTRA REMUNERACION:</label>
                                <select class="form-control"  name="tip" required>
                                  <option value="">--</option>
                                  <option value="SI">SI</option>
                                  <option value="NO">NO</option>
                                </select>
                          </div>
                           <div class="col-lg-2 col-md-3  top">
                             <button type="submit" class="btn btn-primary" name="aggr" value="agre">Configurar </button>
                             <a href="index.php?menu=11" class="btn btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
                           </div>
                           <?php }  if ($tip=='SI') {?>
                             <div class="form-group col-xs-12 col-md-12 col-lg-2">
                               <label>RUC:</small></label>
                               <input class="form-control" placeholder="000000000" name="xruc"  pattern="[0-9+].{6,}" title="Ingrese numeros">
                             </div>
                             <div class="form-group col-xs-12 col-md-12 col-lg-4">
                                <label>EMPRESA / RAZON SOCIAL:</label>
                                <input class="form-control text-uppercase" name="xnom" placeholder="Ingrese Razon Social" pattern="[A-z ]+.{5}" title="Ingrese solo letras" required>
                              </div>
                              <div class="form-group col-xs-12 col-md-12 col-lg-2">
                                <label>OTROS PAGOS (S/):</label>
                                <input class="form-control" placeholder="0.00" name="otrop"  pattern="\d+(\.\d{2})?">
                              </div>
                              <div class="form-group col-xs-12 col-md-12 col-lg-2">
                                <label>PERIODO:</label>
                                <input class="form-control" placeholder="Ej. 2019" name="xper" pattern="[0-9+].{2,}" title="Ingrese numeros" required>
                              </div>
                              <div class="col-lg-3 col-md-3 top">
                                <button type="submit" class="btn btn-primary" name="aggr" value="new">Guardar </button>
                                <button type="reset" class="btn btn-default" name="cancel" value="cancel">Limpiar</button>
                                <a href="index.php?menu=11" class="btn btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
                              </div>
                            <?php } ?>
                            <?php if ($tip=='NO') { ?>
                              <div class="form-group col-xs-12 col-md-12 col-lg-6">
                                  <label>CONDICION:</label>
                                  <input class="form-control text-uppercase" name="con" value="DECLARO QUE NO HE RECIBIDO REMUNERACIONES DE OTRAS EMPRESAS"  disabled>
                              </div>
                              <div class="form-group col-xs-12 col-md-12 col-lg-2">
                                <label>FECHA DE DECLARACION:</label>
                                <input class="form-control input-group-sm" type="date"  name="xfec" id="xfec" required>
                              </div>
                              <div class="col-lg-3 col-md-3 top">
                                <button type="submit" class="btn btn-primary" name="aggr" value="new">Guardar </button>
                                <button type="reset" class="btn btn-default" name="cancel" value="cancel">Limpiar</button>
                                <a href="index.php?menu=11" class="btn btn-warning" role="button"><i class="glyphicon glyphicon-chevron-left"></i></a>
                              </div>
                            <?php } ?>
                        </form>
                        <?php if ($rb<>'') { ?>
                        <div class="table-responsive col-lg-12">
                          <hr class="black" />
                            <h4 align="center" class="azul"><i class="fa fa-inbox fa-fw"></i><strong>Otras Remuneraciones</strong></h4>
                          <hr class="black" />

                           <table class="table table-hover" id="dataTables-example">
                             <thead>
                               <tr>
                                 <th class="text-center">#</th>
                                 <th class="text-center">RUC</th>
                                 <th class="text-center">RAZON SOCIAL</th>
                                 <th class="text-center ">PERIODO</th>
                                 <th class="text-center info">MONTO</th>
                                 <th class="text-center">ACTUALIZAR</th>
                                 <th class="text-center">DECLARACION J.</th>
                              </tr>
                             </thead>
                             <tbody class="text-center">
                             <?php
                             $sql="SELECT ot_id,per_ndoc,ot_ruc,ot_nombre,ot_monto,ot_periodo FROM sys_rh_personal_otros so WHERE per_ndoc='$edit'
                                  AND extract(year from ot_fecha)=extract(year FROM now())
                                  UNION ALL
                                  select se.emp_id,sl.per_ndoc,se.emp_ruc,se.emp_nombre,sl.lab_sueldo, extract(year FROM now()) as periodo
                                  FROM sys_rh_personal_lab sl,sys_empresas  se WHERE sl.per_ndoc='$edit'
                                  and se.emp_id = sl.emp_id";
                             $result=mysqli_query($con,$sql);
                            $nfil = 1;
                             while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
                               <tr>
                                   <td><?php echo $nfil ++; ?></td>
                                   <td><?php echo $row['ot_ruc']; ?></td>
                                   <td><?php echo strtoupper($row['ot_nombre']); ?></td>
                                   <td class="centeralign">
                                      <?php echo $row['ot_periodo']; ?>
                                   </td>
                                   <td class="info">
                                      <?php #echo '<input type="text" value='.number_format($row['ot_monto'], 2,",",".").'>';
                                        echo '<form  role="form" action="index.php?" method="GET">
                                        <input type="hidden" name="menuu" value="6">
                                        <input type="hidden" name="opcion" value="otrosp">
                                        <input type="hidden" name="id" value="'.$row['per_ndoc'].'">
                                        <input class="form-control text-center" name="nmonto" id="nmonto" value="'. number_format($row['ot_monto'], 2,".","").'"
                                         pattern="\d+(\.\d{2})?" title="Ingrese solo numeros con 2 decimales" placeholder="0.00" required>
                                        ';
                                      ?>
                                   </td>
                                 <!--EDITAR-->

                                   <td class="centeralign">
                                     <?php
                                     echo '<button class="btn btn-warning btn-sm glyphicon glyphicon-edit" name="acted" value='.$row['ot_id'].'></button>
                                     </form>';
                                     ?>
                                   </td>
                                   <td class="centeralign">
                                     <form role="form" method="get" action="pdf_documentos/documentos/declara_remunera.php" onSubmit = "return validar(this)"  target="noexiste">
                                     <input type="hidden" name="id" value="<?php echo $xnum; ?>">
                                     <input type="hidden" name="opcion" value="SI">
                                     <input type="hidden" name="empid" value="<?php echo $id_emp; ?>">
                                     <input type="hidden" name="monto" value="<?php echo $row['ot_monto']; ?>">
                                       <button class="btn btn-primary btn-sm glyphicon glyphicon-book" name="aggr" value='<?php echo $row['ot_id']; ?>'></button>
                                     </form>
                                   </td>

                               </tr>
                                 <?php } ?>
                             </tbody>
                           </table>

                          </div>
                        <?php }} ?>


              </div>   <!-- /.body -->
        </div>
    </div>
        <!-- /.col-lg-12 -->
</div>
<!-- /.row uploadImage1-->
<script>
function previewImage(nb) {
    var reader = new FileReader();
    reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);
    reader.onload = function (e) {
        document.getElementById('uploadPreview'+nb).src = e.target.result;
    };
}
</script>

<?php
  include "../Funciones/BD.php";
  #Consultas Basicas
  include "Funciones/Importacion/Listas.php";
  include "Funciones/Importacion/GetMarca.php";
  #Modal
  include "Vistas/Modal/M_Marca.php";
  #include "Vistas/Modal/M_Pais.php";
  #include "Vistas/Modal/M_Puerto.php";
  #include "Vistas/Modal/B_Proveedor.php";
  #include "Vistas/Modal/M_Proveedor.php";
  $id_cat;
 ?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-lg-12">
          <div class="row" id="titulo">
            <div class="col-lg-6">
              <h2 class="azul"><i class="fa fa-folder-open-o fa-fw"></i><strong>Folder de Importaci&oacute;n</strong></h2>
            </div>
            <div class="col-lg-6 text-right" >
              <button class="btn btn-primary" id="show"><i class="fa fa-plus "></i> Agregar</button>
            </div>
          </div>
        </div>
        <div class="row">
           <div class="col-lg-12">
              <div id="titulo2" class="col-lg-6" style="display: none;" >
                <h2 class="azul"><i class="fa fa-folder-open-o fa-fw"></i><strong>Nuevo Folder</strong></h2>
             </div>
           </div>
         </div>
        <div class="table-responsive col-lg-12"><hr class="black" />
            <div id="element2">
             <table class="table table-striped table-bordered table-hover" id="dataTables-example">
               <thead>
                 <tr>
                   <th class="text-center">#</th>
                   <th class="text-center">TIPO DE DOCUMENTO</th>
                   <th class="text-center">NUMERO</th>
                   <th class="text-center">RAZON SOCIAL</th>
                   <th class="text-center">FORMA DE PAGO</th>
                   <th class="text-center">MONEDA</th>
                   <th class="text-center">T.C.</th>
                   <th class="text-center">FECHA</th>
                   <th class="text-center">ESTATUS</th>
                   <th class="text-center">DETALLE</th>
                 </tr>
               </thead>
               <tbody class="text-center">
               <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><form   role="form" action="index.php?" method="get">
                      <input type="hidden" name="menu" value="2">
                      <input type="hidden" name="opc" value="folder">
                      <button class="btn btn-info btn-sm glyphicon glyphicon-list"></button>
                    </form></td>
               </tr>

             </tbody>
           </table>
        </div></div>
        <!--Div Oculto-->
        <div id="content">
           <div id="element" style="display: none;">
            <form name="RegistroE" id="RegistroE" action="index.php?menu="  method="post">
             <div class="form-group  col-md-6 col-xs-12 col-lg-2">
              <label>FOLIO:</label>
                <div class="input-group">
                    <input name="txtanual" type="text" class="form-control" placeholder="Ej: 2019" aria-describedby="basic-addon1" value="<?= date("Y") ?>">
                    <span class="input-group-addon" id="basic-addon1">N°</span>
                    <input name="textnum" type="text" class="form-control" placeholder="Folio" aria-describedby="basic-addon1">
                </div>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                <label>INCOTERM:</label>
                <select name="txtincoterm" class="form-control text-uppercase" required="required" data-live-search='true'>
                  <option value="" disabled selected>--</option>
                  <option value="FOB">FOB</option>
                  <option value="CFR">CFR</option>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                <label>TIPO DE CTN:</label>
                <select name="txttctn"  class="form-control text-uppercase" required="required" data-live-search='true'>
                    <option value="" disabled selected>--</option>
                    <option value="40HC">40HC</option>
                    <option value="40HC">20ST</option>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-1">
                  <label>QTY CTN</label>
                  <input type="text" name="txtqtyctn" class="form-control" id="" required>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                <label >TIPO DE PRODUCTO:</label>

                  <select class="form-control"  name="txtcategoria" id="categoria" onchange="myFunction()" required>
                    <?php foreach ($dataTipoProducto as $tipoProoducto) { ?>
                        <option value="<?= $tipoProoducto->cat_descripcion ?>"><?= $tipoProoducto->cat_descripcion ?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label>MARCA:</label>
                <select class="form-control"  name="txtmarca" id="subcategoria" required></select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                <label>PA&Iacute;S:</label>
                <select name="txtpais" class="form-control">
                    <option value="" disabled selected>--</option>
                    <?php foreach ($dataPais as $pais) { ?>
                        <option value="<?= $pais->id ?>"><?= $pais->nombre ?></option>
                    <?php }
                     echo '<option data-toggle="modal"  data-target="#M_Pais">AÑADIR</option>'; ?>
                </select>
              </div>
              <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                  <label>PUERTO:</label>
                  <select name="txtpuerto" class="form-control selectpicker show-tick">
                      <option value="" disabled selected>--</option>
                      <?='<option data-toggle="modal" data-target="#M_Puerto">AÑADIR</option>'; ?>
                  </select>
              </div>
                <div class="col-lg-6  col-md-6" >
                  <label>Proveedor</label>
                  <div class="row">
                      <div class="col-xs-10 col-md-10" style="padding-right:0 !important">
                          <input type="text" class="form-control" readonly name="proveedor" id="proveedor" onclick="BuscarProveedor()">
                          <input type="hidden" name="provee_int_id" id="provee_int_id" class="form-control" value="">
                          <small id="helpId" class="text-muted text-justify">Haga doble clic en la casilla para buscar un proveedor</small>
                      </div>
                      <div class="col-xs-2 col-md-1">
                          <button data-target="#M_Proveedor" data-toggle="modal" type="button" id="nproveedor" class="btn btn-primary">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                          </button>
                      </div>
                  </div>
                </div>
                <div class="form-group  col-md-6 col-xs-12 col-lg-3">
                    <label class="control-label" for="">#Proforma:</label>
                    <input name="txtproforma" class="form-control" type="text" required>
                </div>
                <div class="form-group  col-md-6 col-xs-12 col-lg-2">
                    <label>Fecha/Proforma:</label>
                    <input class="form-control" type="date" required>
                </div>
                <div class="col-lg-3 col-md-4 hidden-xs top">
                 <button type="submit" class="btn btn-sm btn-primary" name="Guardar" value="Insertar">Guardar </button>
                 <button type="reset" class="btn btn-sm btn-default">Limpiar</button>
                 <a href="#" class="btn btn-sm btn-warning" id="hide"><i class="glyphicon glyphicon-chevron-left"></i></a>
               </div>
              </form>
           </div>
         </div><!---FIN DIV OCULTO-->
      </div><!--pbody -->
    </div><!--pdefaul -->
  </div><!--col -->
</div><!--row -->
<script>
 $(document).ready(function () {
  //SELECT DEPENDIENTE
  $("#categoria").on('change', function () {
      $("#categoria option:selected").each(function () {
          var id_categoria = $(this).val();
          $.post("Funciones/Importacion/GetMarca.php", { id_categoria: id_categoria }, function(data) {
              $("#subcategoria").html(data);
          });
      });
    });
  });
  function BuscarProveedor() {
    $("#B_Proveedor").modal("show");
  }
</script>
<script language="javascript">
    function myFunction() {
      var y = document.getElementById("categoria").value;
      //var x = document.getElementById("xhasta").value;
      //document.getElementById("demo2").value = "PLANILLA DE REMUNERACIONES DESDE: " + y + " HASTA: " + x;
      var texto = y ;
      //var texto1 = x ;
      var salida = formato(texto);
      var variableJS = salida

      //var salida1 = formato(texto1);
      document.getElementById("demo").value = salida;

      //document.getElementById("demo2").value = "PLANILLA DE REMUNERACIONES DESDE: " + salida + " HASTA: " + salida1;
      function formato(texto){
        return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
        }
    }
</script>
<script>

function enviar()
{
  // Esta es la variable que vamos a pasar
  var miVariableJS=$("#categoria").val();

  $("#categoria").load("Vistas/Modal/M_Marca.php", {nombre: miVariableJS, edad: 45}, function(){
      $("#subcat").html();
     });
}

</script>
